#pragma once

#include "GL.h"

#include "TextureLoader.h"
#include "Camera.h"
#include "ShaderProgram.h"
#include "DirectionalLight.h"

#include <string>

namespace pndev
{
	class SkyBox
	{
	public:
		SkyBox(const ShaderProgram& skyshader, const Texture& day, const Texture& night, float dayLength);
		~SkyBox();

		void renderSkybox(const Camera& camera, const DirectionalLight& light);

	private:

		void loadSkybox(const Texture& day, const Texture& night, float dayLength);

		Texture day_texture_;
		Texture night_texture_;

		GLuint cube_vao_;

		ShaderProgram skybox_shader_;

	};
}