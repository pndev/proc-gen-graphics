#pragma once
#include <vector>
#include <string>

#include "Camera.h"
#include "SkyBox.h"
#include "DirectionalLight.h"
#include "Object.h"
#include "Gui/GuiElement.h"
#include "Shadowmap.h"
#include "Scripts/Script.h"
#include "InputManager.h"

namespace pndev
{
	struct SceneSettings
	{
		glm::ivec2 shadowmapResolution;
	};

	/*
		A scene that holds the objects and gui to be rendered.
		Supports light, shadows, skybox, multiple cameras.
	*/
	class Scene
	{
	public:

		Scene(std::string name, const pndev::SceneSettings& settings);
		~Scene();

		void setInputManager(InputManager* inputManager);

		void initialize();
		void update();
		void render(int screenWidth, int screenHeight);
		void destroy();

		void setActiveCamera(int cameraIndex);
		Camera* getActiveCamera() const;

		void addCamera(Camera* camera);
		Camera* getCamera(int index) const;

		std::vector<Camera*> getCameras();

		void addObject(Object* obj);
		void addGuiElement(GuiElement* gui);
		void addScript(Script* script);

		const std::vector<Object*>& getObjects() const;
		const std::vector<GuiElement*>& getGuiElements() const;
		const std::vector<Script*>& getScripts() const;

		void setSkybox(SkyBox* skybox);
		SkyBox* getSkybox() const;

		void setLight(DirectionalLight* light);
		DirectionalLight* getLight() const;

		const std::string& getName() const;

		InputManager* getInputManager() const;

	private:

		InputManager* input_manager_;

		bool initialized_;

		Camera* active_camera_;

		const std::string scene_name_;
		std::vector<Object*> objects_;
		std::vector<GuiElement*> gui_elements_;
		std::vector<Camera*> cameras_;
		std::vector<Script*> scripts_;

		SkyBox* skybox_;
		DirectionalLight* light_;

		Shadowmap shadow_map_;
	};
}