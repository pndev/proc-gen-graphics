#include "BasicControl.h"

#include "../Time.h"

pndev::BasicControl::BasicControl() :
	Script("control"),
	key_callback_id_(-1),
	mouse_callback_id_(-1),
	stopped_(false)
{
}

pndev::BasicControl::~BasicControl()
{
	
}

void pndev::BasicControl::initialize(Scene& scene)
{
	key_callback_id_ = scene.getInputManager()->registerKeyCallback(
		[this](int key, int scancode, int action, int mods) { keyCallback(key, scancode, action, mods); }
	);
}

void pndev::BasicControl::update(Scene & scene)
{
	cameraMovement(scene.getActiveCamera(), Time::getRealDeltaTime());
	cameraRotation(scene.getActiveCamera(), scene.getInputManager()->getMouseInfo());
}

void pndev::BasicControl::destroy(Scene& scene)
{
	if (key_callback_id_ != -1 && scene.getInputManager() != nullptr)
	{
		scene.getInputManager()->unregisterKeyCallback(key_callback_id_);
	}
}

void pndev::BasicControl::setCameraSpeed(float speed)
{
	camera_state_.camera_speed = speed;
}

float pndev::BasicControl::getCameraSpeed() const
{
	return camera_state_.camera_speed;
}

void pndev::BasicControl::cameraMovement(pndev::Camera* cam, float deltaSecs)
{
	if (camera_state_.movement_state == CameraMovementFlags::IDLE)
		return;

	if (cam != nullptr)
	{
		if (camera_state_.movement_state & CameraMovementFlags::FORWARD)
		{
			cam->setPosition(cam->getPosition() + (cam->getForward()*deltaSecs*camera_state_.camera_speed*camera_state_.speed_multiplier));
		}

		if (camera_state_.movement_state & CameraMovementFlags::BACKWARD)
		{
			cam->setPosition(cam->getPosition() - (cam->getForward()*deltaSecs*camera_state_.camera_speed*camera_state_.speed_multiplier));
		}

		if (camera_state_.movement_state & CameraMovementFlags::RIGHT)
		{
			cam->setPosition(cam->getPosition() + (cam->getRight()*deltaSecs*camera_state_.camera_speed*camera_state_.speed_multiplier));
		}

		if (camera_state_.movement_state & CameraMovementFlags::LEFT)
		{
			cam->setPosition(cam->getPosition() - (cam->getRight()*deltaSecs*camera_state_.camera_speed*camera_state_.speed_multiplier));
		}
	}
}

void pndev::BasicControl::cameraRotation(pndev::Camera* cam, const MouseData& mouseData)
{
	if (cam != nullptr && mouseData.right_pressed)
	{
		float dh, dv;
		int w = 1, h = 1;
		cam->getViewPort(w, h);
		dh = (float) mouseData.delta_x / w * glm::pi<float>();
		dv = (float) mouseData.delta_y / h * glm::pi<float>();
		cam->rotateByDelta(dh, dv);
	}
}

void pndev::BasicControl::keyCallback(int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_W || key == GLFW_KEY_UP)
	{
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
		{
			camera_state_.movement_state = camera_state_.movement_state | CameraMovementFlags::FORWARD;
		}
		else if (action == GLFW_RELEASE)
		{
			camera_state_.movement_state = camera_state_.movement_state & ~CameraMovementFlags::FORWARD;
		}
	}

	if (key == GLFW_KEY_S || key == GLFW_KEY_DOWN)
	{
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
		{
			camera_state_.movement_state = camera_state_.movement_state | CameraMovementFlags::BACKWARD;
		}
		else if (action == GLFW_RELEASE)
		{
			camera_state_.movement_state = camera_state_.movement_state & ~CameraMovementFlags::BACKWARD;
		}
	}

	if (key == GLFW_KEY_A || key == GLFW_KEY_LEFT)
	{
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
		{
			camera_state_.movement_state = camera_state_.movement_state | CameraMovementFlags::LEFT;
		}
		else if (action == GLFW_RELEASE)
		{
			camera_state_.movement_state = camera_state_.movement_state & ~CameraMovementFlags::LEFT;
		}
	}

	if (key == GLFW_KEY_D || key == GLFW_KEY_RIGHT)
	{
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
		{
			camera_state_.movement_state = camera_state_.movement_state | CameraMovementFlags::RIGHT;
		}
		else if (action == GLFW_RELEASE)
		{
			camera_state_.movement_state = camera_state_.movement_state & ~CameraMovementFlags::RIGHT;
		}
	}

	if (key == GLFW_KEY_P && action == GLFW_PRESS)
	{
		stopped_ = !stopped_;

		static float savedscale = 1.0f;

		if (stopped_)
		{
			savedscale = Time::getScale();
			Time::setScale(0.0f);
		}
		else
		{
			Time::setScale(savedscale);
		}
	}
	
	if (mods == GLFW_MOD_SHIFT)
	{
		camera_state_.speed_multiplier = 4.0f;
	}
	else 
	{
		camera_state_.speed_multiplier = 1.0f;
	}
}

void pndev::BasicControl::mouseCallback(int key, int action, int mods)
{
}
