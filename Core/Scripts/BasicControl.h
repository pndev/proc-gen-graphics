#pragma once

#include "Script.h"
#include "../Camera.h"
#include "../Scene.h"

#include <glm\glm.hpp>

namespace pndev
{
	class BasicControl : public Script
	{
	public:

		BasicControl();
		~BasicControl();

		void initialize(Scene& scene);
		void update(Scene& scene);
		void destroy(Scene& scene);

		void setCameraSpeed(float speed);
		float getCameraSpeed() const;

	private:

		struct CameraMovementFlags
		{
			static const int IDLE = 0;
			static const int FORWARD = 1;
			static const int BACKWARD = 2;
			static const int RIGHT = 4;
			static const int LEFT = 8;
		};

		struct CameraState
		{
			int movement_state = CameraMovementFlags::IDLE;
			float camera_speed = 5.0f;
			float speed_multiplier = 1.0f;
		};

		CameraState camera_state_;
		int key_callback_id_;
		int mouse_callback_id_;

		bool stopped_;

		void cameraMovement(pndev::Camera* cam, float deltaSecs);
		void cameraRotation(pndev::Camera* cam, const MouseData& mouseData);

		void keyCallback(int key, int scancode, int action, int mods);
		void mouseCallback(int key, int action, int mods);
	};
}