#include "DynamicLight.h"

#include "../DirectionalLight.h"
#include "../Scene.h"
#include "../Time.h"

pndev::DynamicLight::DynamicLight(float dayLength, const glm::vec3& dayColor, const glm::vec3& nightColor) :
	Script("light"),
	day_length_(dayLength),
	day_color_(dayColor),
	night_color_(nightColor),
	animate_(true),
	angle_(0.0f)
{
	day_length_ = glm::max(day_length_, 1.0f);
}

pndev::DynamicLight::~DynamicLight()
{

}

void pndev::DynamicLight::initialize(Scene& scene)
{
	scene.getInputManager()->registerKeyCallback(
		[this, &scene](int key, int scan, int action, int mods)
	{
		if (key == GLFW_KEY_KP_ADD && action == GLFW_PRESS) {
			day_length_++;
		}
		else if (key == GLFW_KEY_KP_SUBTRACT && action == GLFW_PRESS) {
			day_length_--;
		}
		else if (key == GLFW_KEY_KP_SUBTRACT && action == GLFW_PRESS) {
			day_length_--;
		}
		else if (key == GLFW_KEY_L && action == GLFW_PRESS)
		{
			animate_ = !animate_;
		}

		day_length_ = glm::max(day_length_, 1.0f);
	});
}

void pndev::DynamicLight::update(Scene& scene)
{
	if (!animate_) return;

	DirectionalLight* light = scene.getLight();
	
	if (light == nullptr) return;

	float dt = Time::getDeltaTime();
	angle_ += 2.0f * glm::pi<float>()* dt / day_length_;

	glm::vec3 dir = glm::vec3(cos(angle_), sin(angle_), 0.0f);
	light->setDirection(dir);

	float t = abs(glm::clamp(sin(angle_), -1.0f, 0.0f));

	light->setDiffuseColor(day_color_ * t + night_color_ * (1.0f - t));
}

void pndev::DynamicLight::destroy(Scene & scene)
{
}

float pndev::DynamicLight::getDayLength() const
{
	return day_length_;
}

void pndev::DynamicLight::setDayLength(float dayLength)
{
	day_length_ = dayLength;
}

glm::vec3 pndev::DynamicLight::getNightColor() const
{
	return night_color_;
}

void pndev::DynamicLight::setNightColor(const glm::vec3 & nightColor)
{
	night_color_ = nightColor;
}

glm::vec3 pndev::DynamicLight::getDayColor() const
{
	return day_color_;
}

void pndev::DynamicLight::setDayColor(const glm::vec3 & dayColor)
{
	day_color_ = dayColor;
}
