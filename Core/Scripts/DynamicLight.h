#pragma once

#include "Script.h"

#include <glm\glm.hpp>

namespace pndev
{
	class DynamicLight : public Script
	{
	public:
		DynamicLight(float dayLength, const glm::vec3& dayColor, const glm::vec3& nightColor);
		~DynamicLight();

		void initialize(Scene& scene);
		void update(Scene& scene);
		void destroy(Scene& scene);

		float getDayLength() const;
		void setDayLength(float dayLength);

		glm::vec3 getNightColor() const;
		void setNightColor(const glm::vec3& nightColor);

		glm::vec3 getDayColor() const;
		void setDayColor(const glm::vec3& dayColor);

	private:

		bool animate_; 
		float angle_;

		float day_length_;
		glm::vec3 day_color_; 
		glm::vec3 night_color_;
	};
}