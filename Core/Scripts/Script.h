#pragma once

#include <string>

namespace pndev
{
	class Scene;

	class Script
	{
	public:
		Script(const std::string& id);
		virtual ~Script();

		virtual void initialize(Scene& scene) = 0;
		virtual void update(Scene& scene) = 0;
		virtual void destroy(Scene& scene) = 0;

	private:

		std::string id_;
	};
}