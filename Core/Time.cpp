#include "Time.h"

#include <GLFW\glfw3.h>

double pndev::Time::elapsed_time_ = 0;
double pndev::Time::delta_time_= 0;
double pndev::Time::frame_start_ = 0;
double pndev::Time::scale_ = 1;
double pndev::Time::real_elapsed_time_ = 0;
double pndev::Time::real_delta_time_ = 0;

double pndev::Time::getDeltaTime()
{
	return delta_time_;
}

double pndev::Time::getElapsedTime()
{
	return elapsed_time_;
}

void pndev::Time::init()
{
	frame_start_ = glfwGetTime();
	elapsed_time_ = 0;
	delta_time_ = 0;
	frame_start_ = 0;
	real_elapsed_time_ = 0;
	real_delta_time_ = 0;
	scale_ = 1;
}

void pndev::Time::calcFrameTime()
{
	double cur = glfwGetTime();
	real_delta_time_ = cur - frame_start_;
	delta_time_ = scale_ * real_delta_time_;
	frame_start_ = cur;
	real_elapsed_time_ += real_delta_time_;
	elapsed_time_ += delta_time_;
}

double pndev::Time::getRealDeltaTime()
{
	return real_delta_time_;
}

double pndev::Time::getRealElapsedTime()
{
	return real_elapsed_time_;
}

double pndev::Time::getScale()
{
	return scale_;
}

void pndev::Time::setScale(double scale)
{
	scale_ = scale;
}

pndev::Time::Time()
{
}


pndev::Time::~Time()
{
}
