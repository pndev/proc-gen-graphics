#include "Scene.h"

pndev::Scene::Scene(
	std::string name,
	const pndev::SceneSettings& settings
	) : 
	scene_name_(name),
	skybox_(nullptr),
	active_camera_(nullptr),
	shadow_map_(settings.shadowmapResolution.x, 
		settings.shadowmapResolution.y),
	initialized_(false),
	input_manager_(nullptr)
{
}

pndev::Scene::~Scene()
{
}

void pndev::Scene::setInputManager(InputManager* inputManager)
{
	input_manager_ = inputManager;
}

void pndev::Scene::initialize()
{
	for (auto obj : objects_)
	{
		obj->initialize(*this);
	}
	for (auto script : scripts_)
	{
		script->initialize(*this);
	}

	initialized_ = true;
}

const std::string& pndev::Scene::getName() const
{
	return scene_name_;
}

pndev::InputManager * pndev::Scene::getInputManager() const
{
	return input_manager_;
}

void pndev::Scene::update()
{
	for (auto object : objects_) 
	{
		object->update(*this);
	}
	
	for (auto script : scripts_)
	{
		script->update(*this);
	}
}

void pndev::Scene::render(int w, int h)
{
	if (active_camera_ == nullptr)
	{
		return;
	}

	active_camera_->update(w, h);

	bool shadows = light_ != nullptr && light_->castShadows();
	if (shadows)
	{
		shadow_map_.computeShadowmap(light_->getDirection(), objects_);
	}

	glViewport(0, 0, w, h);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (skybox_ != nullptr) skybox_->renderSkybox(*active_camera_, *light_);

	glm::mat4 viewMatrix = active_camera_->getViewMatrix();
	glm::mat4 projectionMatrix = active_camera_->getProjectionMatrix();

	glm::mat4 lightMatrix = shadow_map_.getLigthMatrix();
	glm::vec3 lightDirection = light_->getDirection();
	glm::vec3 lightDiffuse = light_->getDiffuseColor();
	glm::vec3 lightAmbient = light_->getAmbientColor();

	for (auto object : objects_)
	{
		object->setTexture("depth_texture", shadow_map_.getDepthTexture());

		for (auto shader : object->getShaders())
		{
			shader.setProperty("global_shadows", shadows);
			shader.setProperty("view_matrix", viewMatrix);
			shader.setProperty("projection_matrix", projectionMatrix);
			shader.setProperty("light_vp", lightMatrix);

			shader.setProperty("directional_light.direction", lightDirection);
			shader.setProperty("directional_light.diffuse", lightDiffuse);
			shader.setProperty("directional_light.ambient", lightAmbient);
		}

		object->render();
	}

	for (auto elem : gui_elements_)
	{
		elem->render(*this);
	}
}

void pndev::Scene::destroy()
{
	for (auto elem : gui_elements_)
	{
		elem->destroy(*this);
	}
	for (auto script : scripts_)
	{
		script->destroy(*this);
	}
}

void pndev::Scene::setActiveCamera(int index)
{
	if (index >= 0 && index < cameras_.size())
	{
		active_camera_ = cameras_[index];
	}
	else if (cameras_.size() > 0)
	{
		active_camera_ = cameras_[0];
	}
}

void pndev::Scene::addCamera(Camera* camera)
{
	if (camera != nullptr)
	{
		cameras_.push_back(camera);

		if (active_camera_ == nullptr)
		{
			active_camera_ = camera;
		}
	}
}

void pndev::Scene::addObject(Object* obj)
{
	if (obj != nullptr)
	{
		objects_.push_back(obj);

		if (initialized_)
		{
			obj->initialize(*this);
		}
	}
}

void pndev::Scene::addGuiElement(GuiElement* gui)
{
	if (gui != nullptr)
	{
		gui_elements_.push_back(gui); 
		if (initialized_)
		{
			gui->initialize(*this);
		}
	}
}

void pndev::Scene::addScript(Script* script)
{
	if (script != nullptr)
	{
		scripts_.push_back(script);
		if (initialized_)
		{
			script->initialize(*this);
		}
	}
}

const std::vector<pndev::Object*>& pndev::Scene::getObjects() const
{
	return objects_;
}

const std::vector<pndev::GuiElement*>& pndev::Scene::getGuiElements() const
{
	return gui_elements_;
}

const std::vector<pndev::Script*>& pndev::Scene::getScripts() const
{
	return scripts_;
}

pndev::Camera* pndev::Scene::getActiveCamera() const
{
	return active_camera_;
}

pndev::Camera* pndev::Scene::getCamera(int index) const
{
	if (index < cameras_.size() && index >= 0)
		return cameras_[index];
	return nullptr;
}

std::vector<pndev::Camera*> pndev::Scene::getCameras()
{
	return cameras_;
}

void pndev::Scene::setSkybox(SkyBox* skybox)
{
	this->skybox_ = skybox;
}

pndev::SkyBox* pndev::Scene::getSkybox() const
{
	return skybox_;
}

void pndev::Scene::setLight(DirectionalLight* light)
{
	this->light_ = light;
}

pndev::DirectionalLight* pndev::Scene::getLight() const
{
	return light_;
}