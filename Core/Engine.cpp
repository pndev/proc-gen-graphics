#include "Engine.h"

#include <algorithm>

#include "Time.h"

#include "Config\Default\SceneLoader.h"
#include "Config\Default\CameraLoader.h"
#include "Config\Default\SkyboxLoader.h"
#include "Config\Default\LightLoader.h"

#include "Config\Default\TerrainLoader.h"
#include "Config\Default\LightScriptLoader.h"
#include "Config\Default\ControlScriptLoader.h"


#define PI 3.141592f

pndev::Engine* pndev::Engine::engine_ = nullptr;

pndev::Engine::Engine(const ContextSettings& settings) :
	settings_(settings)
{
	glfwSetErrorCallback(errorCallback);

	if (GLFW_TRUE != glfwInit())
	{
		throw std::exception("GLFW initialization failed!");
	}

	std::cout << "GLFW initialized\n";

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, settings_.opengl_major_version);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, settings_.opengl_minor_version);
	window_ = glfwCreateWindow(settings_.window_width, settings_.window_height, settings_.window_title.c_str(), nullptr, nullptr);
	if (!window_)
	{
		throw std::exception("Window initialization failed!");
	}

	glfwMakeContextCurrent(window_);

	const GLenum err = glewInit();

	if (GLEW_OK != err)
	{
		errorCallback(err, (const char*)(glewGetErrorString(err)));
		throw std::exception("GLEW initialization failed");
	}

	std::cout << "OpenGL initialized!\n";
	std::cout << "OpenGL version: " << (const char*)glGetString(GL_VERSION) << "\n";
	std::cout << "GLSL version: " << (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
	std::cout << "OpenGL vendor: " << (const char*)glGetString(GL_VENDOR) << "\n";

	input_manager_ = new InputManager(window_);

	glfwSetFramebufferSizeCallback(window_, framebufferSizeCallback);
	glfwSetKeyCallback(window_, keyCallback);
	glfwSetMouseButtonCallback(window_, mouseButtonCallback);
	glfwSetDropCallback(window_, fileDropcallback);

	engine_ = this;

	camera_loader_.registerDefaultConfiguration(CameraLoader());
	skybox_loader_.registerDefaultConfiguration(SkyboxLoader(texture_loader_));
	light_loader_.registerDefaultConfiguration(LightLoader());

	object_loader_.registerConfiguration("terrain", TerrainLoader(texture_loader_));
	script_loader_.registerConfiguration("light", LightScriptLoader());
	script_loader_.registerConfiguration("control", ControlScriptLoader());

	scene_loader_.registerDefaultConfiguration(
		SceneLoader(
			camera_loader_,
			object_loader_,
			script_loader_,
			gui_loader_,
			light_loader_,
			skybox_loader_
		)
	);

	/*
		Loads the default shaders.
	*/
	ShaderProgram("terrain", settings_.resource_path + "Shader/terrainvertex.glsl", "Shader/terrainfragment.glsl");
	ShaderProgram("grass", settings_.resource_path + "Shader/grassvertex.glsl", "Shader/grassfragment.glsl");
	ShaderProgram("sky", settings_.resource_path + "Shader/skyvert.glsl", "Shader/skyfrag.glsl");
	ShaderProgram("shadow", settings_.resource_path + "Shader/shadowvertex.glsl", "Shader/shadowfragment.glsl");
	ShaderProgram("blur", settings_.resource_path + "Shader/fullquadvertex.glsl", "Shader/blurfragment.glsl");

	current_scene_ = nullptr;
}

pndev::Engine::~Engine()
{
	for (auto scene : scenes_) 
	{
		scene->destroy();
	}

	glfwDestroyWindow(window_);
	glfwTerminate();

	delete input_manager_;
}

void pndev::Engine::run()
{
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glfwSwapInterval(0);

	pndev::Time::init();

	while (!glfwWindowShouldClose(window_))
	{
		pndev::Time::calcFrameTime();
		input_manager_->update();

		glViewport(0, 0, settings_.window_width, settings_.window_height);

		if (current_scene_ != nullptr)
		{
			current_scene_->update();
			current_scene_->render(settings_.window_width, settings_.window_height);
		}

		glfwSwapBuffers(window_);
	}
}

pndev::Scene* pndev::Engine::loadScene(const std::string& sceneConfig)
{
	Scene* scene = scene_loader_.loadConfigurationFromFile(sceneConfig);
	scenes_.push_back(scene);
	scene->setInputManager(input_manager_);
	return scene;
}

void pndev::Engine::makeCurrent(const std::string& sceneName)
{
	if (current_scene_ != nullptr && current_scene_->getName() == sceneName)
	{
		return;
	}

	if (current_scene_ != nullptr)
	{
		current_scene_->destroy();
	}

	auto sceneIter = std::find_if(scenes_.begin(), scenes_.end(), 
		[&sceneName](Scene* arg)
		{
			return arg!= nullptr && arg->getName() == sceneName;
		});

	if (sceneIter != scenes_.end())
	{
		current_scene_ = *sceneIter;
		current_scene_->initialize();
	}
}

void pndev::Engine::errorCallback(int error, const char* description)
{
	std::cerr << "Error code " << error << ": " << description << "\n";
}

void pndev::Engine::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);

	engine_->input_manager_->keyCallback(key, scancode, action, mods);
}

void pndev::Engine::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	engine_->input_manager_->mouseCallback(button, action, mods);
}

void pndev::Engine::fileDropcallback(GLFWwindow * window, int files, const char ** filenames)
{
	if (files != 1) return;
	engine_->makeCurrent(engine_->loadScene(filenames[0])->getName());
}

void pndev::Engine::framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
	engine_->settings_.window_width = width;
	engine_->settings_.window_height = height;
}