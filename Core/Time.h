#pragma once

namespace pndev 
{

	class Time
	{
	public:

		/*
			Delta time with scale applied
		*/
		static double getDeltaTime();

		/*
			Elapsed time with scale applied
		*/
		static double getElapsedTime();
		
		/*
			Delta time without scale applied
		*/
		static double getRealDeltaTime();

		/*
			Elapsed time without scale applied
		*/
		static double getRealElapsedTime();

		static void init();
		static void calcFrameTime();

		static double getScale();
		static void setScale(double scale);

	private:
		Time();
		~Time();

		static double real_elapsed_time_;
		static double real_delta_time_;
		static double scale_;
		static double elapsed_time_;
		static double delta_time_;
		static double frame_start_;
	};

}