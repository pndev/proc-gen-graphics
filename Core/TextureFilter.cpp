#include "TextureFilter.h"

#include <glm\glm.hpp>

pndev::TextureFilter::TextureFilter(const std::vector<GLuint>& outputTextures, const pndev::ShaderProgram& shader) :
	shader_(shader)
{
	GLint maxAttach = 0;
	glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxAttach);
	maxAttach = maxAttach <= outputTextures.size() ? maxAttach : outputTextures.size();

	std::vector<GLuint> attachments;

	glGenFramebuffers(1, &filter_fbo_);
	glBindFramebuffer(GL_FRAMEBUFFER, filter_fbo_);

	for (int i = 0; i < maxAttach; i++)
	{
		texture_output_.push_back(outputTextures[i]);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, texture_output_[i], 0);
		attachments.push_back(GL_COLOR_ATTACHMENT0 + i);
	}

	glDrawBuffers(attachments.size(), attachments.data());

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "Texturefilter framebuffer init failed!" << std::endl;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glm::vec2 vertices[] =
	{
		glm::vec2(-1, -1),
		glm::vec2(1, -1),
		glm::vec2(1, 1),
		glm::vec2(-1, 1)
	}; 
	
	int indices[] =	{ 0, 1, 2, 0, 2, 3 };

	GLuint vbo;
	GLuint ebo;

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &filter_vao_);

	glBindVertexArray(filter_vao_);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

	glBindVertexArray(0);
}

pndev::TextureFilter::~TextureFilter()
{

}

const std::vector<GLuint>& pndev::TextureFilter::getFilteredTextureId()
{
	return texture_output_;
}

void pndev::TextureFilter::applyFilter(GLuint texture, int width, int height)
{
	GLint viewportSaved[4];
	glGetIntegerv(GL_VIEWPORT, viewportSaved);
	glViewport(0, 0, width, height);

	glBindFramebuffer(GL_FRAMEBUFFER, filter_fbo_);
	glClear(GL_COLOR_BUFFER_BIT);

	shader_.use();

	GLuint ot = glGetUniformLocation(shader_.getProgram(), "original_texture");
	GLuint w = glGetUniformLocation(shader_.getProgram(), "texture_width");
	GLuint h = glGetUniformLocation(shader_.getProgram(), "texture_height");

	glUniform1i(ot, 0);
	glUniform1i(w, width);
	glUniform1i(h, height);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glBindVertexArray(filter_vao_);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);

	glBindFramebuffer(GL_FRAMEBUFFER, GL_NONE);
	glViewport(viewportSaved[0], viewportSaved[1], viewportSaved[2], viewportSaved[3]);
}
