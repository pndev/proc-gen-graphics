#pragma once

#include "GL.h"

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\quaternion.hpp>

#include <string>

#include "TextureLoader.h"

namespace pndev
{
	/*
		Projective 3D camera.

	*/
	class Camera
	{
	public:

		/*
			Identity view matrix camera.
		*/
		Camera();

		/*
			Identity view matrix camera with name.
		*/
		Camera(const std::string& name);

		/*
			Identity view matrix camera with empty name.
		*/
		~Camera();

		/*
			Sets the width and height of the viewport.
		*/
		void update(int width, int height);


		/*
			Parameter getters.
		*/

		void getViewPort(int& width, int& height) const;
		void getParameters(float & fov, float & near, float & far) const;

		glm::vec3 getPosition() const;
		glm::vec3 getForward() const;
		glm::vec3 getRight() const;

		/*
			View and projection matrix calculation given the internal state of the camera.
		*/

		glm::mat4 getViewMatrix() const;
		glm::mat4 getProjectionMatrix() const;

		/*
			Camera parameter setters.
		*/

		void setParameters(float fov, float near, float far);

		/* Moves the camera to the given position. */
		void setPosition(const glm::vec3& position);

		/* Directs the towards the target point. */
		void lookAt(const glm::vec3& target);

		/* rotates the camera horizontally and vertically */
		void rotateByDelta(float horizontal, float vertical);

	private:

		const std::string name_;

		int width_;
		int height_;

		glm::vec3 position_;
		glm::vec3 forward_;
		glm::vec3 up_;

		float fov_;
		float near_;
		float far_;

	};

}
