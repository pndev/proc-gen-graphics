#pragma once

#include "../Camera.h"

namespace pndev {

	class Scene;

	/*
		Abstract GUI element
	*/
	class GuiElement
	{
	public:
		GuiElement();
		~GuiElement();

		virtual void initialize(const Scene& scene) = 0;
		virtual void processEvents(const Scene& scene) = 0;
		virtual void render(const Scene& scene) = 0;
		virtual void destroy(const Scene& scene) = 0;
	};

}