#pragma once

#include "GL.h"

#include "ShaderProgram.h"
#include "TextureLoader.h"

#include <vector>

namespace pndev 
{
	class TextureFilter
	{
	public:
		TextureFilter(const std::vector<GLuint>& outputTextures, const ShaderProgram& shader);
		~TextureFilter();

		void applyFilter(GLuint texture, int width, int height);

		const std::vector<GLuint>& getFilteredTextureId();

	private:

		std::vector<GLuint> texture_output_;

		ShaderProgram shader_;
		GLuint filter_fbo_;
		GLuint filter_vao_;

	};
}