#include "Shadowmap.h"

#include "..\Procedural\Cube.h"
#include "Scene.h"

pndev::Shadowmap::Shadowmap(int width, int height) :
	blur_filter_(nullptr),
	shadowmap_shader_(*ShaderProgram::get("shadow")),
	blur_shader_(*ShaderProgram::get("blur")),
	width_(width),
	height_(height)
{
	initFbo();

	glGenTextures(1, &filtered_depth_texture_);
	glBindTexture(GL_TEXTURE_2D, filtered_depth_texture_);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, width_, height_, 0, GL_RG, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);

	std::vector<GLuint> outputTextures;
	outputTextures.push_back(filtered_depth_texture_);

	blur_filter_ = new TextureFilter(outputTextures, blur_shader_);
}


pndev::Shadowmap::~Shadowmap()
{
	if (blur_filter_ != nullptr) delete blur_filter_;
}

void pndev::Shadowmap::computeShadowmap(const glm::vec3& lightDirection, const std::vector<Object*>& objects)
{
	/*
		Calculating the light position:
			Every object must be fully in the frustum.
			Calculate the minimal AABB which contains every object.
			Draw the circumsphere.
			The light position is where the inverse light vector (scaled) crosses the sphere.
	*/

	BoundingBox scenebb = *objects[0]->getBoundingBox();

	for (int i = 1; i < objects.size(); i++)
	{
		BoundingBox* bb = objects[i]->getBoundingBox();
		if (bb != nullptr)
		{
			scenebb.scaleToContain(bb->getBasePoint());
			scenebb.scaleToContain(bb->getBasePoint() + bb->getSides());
		}
	}

	glm::vec3 base = scenebb.getBasePoint() + scenebb.getSides() / 2.0f;

	float diag = glm::length(scenebb.getSides()) / 2.0f + 1.0f;
	glm::mat4 lightView = glm::lookAt(base - glm::normalize(lightDirection)*diag, base, glm::vec3(0, 1, 0));
	float w = glm::max(glm::max(scenebb.getSides().x, scenebb.getSides().y), glm::max(scenebb.getSides().z, diag));
	glm::mat4 lightProjection = glm::ortho(-w, w, -w, w, 0.0f, 2*diag);

	light_matrix_ = lightProjection * lightView;

	GLint viewportSaved[4];
	glGetIntegerv(GL_VIEWPORT, viewportSaved);
	glViewport(0, 0, width_, height_);

	glBindFramebuffer(GL_FRAMEBUFFER, shadow_fbo_);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (int i = 0; i < objects.size(); i++)
	{
		objects[i]->renderShadowmap(shadowmap_shader_, lightView, lightProjection);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(viewportSaved[0], viewportSaved[1], viewportSaved[2], viewportSaved[3]);

	blur_filter_->applyFilter(depth_texture_, width_, height_);
}

GLuint pndev::Shadowmap::getDepthTexture()
{
	return filtered_depth_texture_;
}

glm::mat4 pndev::Shadowmap::getLigthMatrix()
{
	return light_matrix_;
}

void pndev::Shadowmap::initFbo()
{
	glGenTextures(1, &depth_texture_);
	glBindTexture(GL_TEXTURE_2D, depth_texture_);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, width_, height_, 0, GL_RG, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);

	GLuint depthRender;
	glGenRenderbuffers(1, &depthRender);
	glBindRenderbuffer(GL_RENDERBUFFER, depthRender);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width_, height_);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glGenFramebuffers(1, &shadow_fbo_);
	glBindFramebuffer(GL_FRAMEBUFFER, shadow_fbo_);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, depth_texture_, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRender);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "Shadowmap framebuffer init failed!" << std::endl;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
