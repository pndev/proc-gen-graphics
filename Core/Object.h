#pragma once

#include "GL.h"

#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>

#include <map>
#include <string>

#include "ShaderProgram.h"
#include "BoundingBox.h"
#include "TextureLoader.h"

namespace pndev
{
	class Scene;

	/*
		It holds the general transform of a 3D object (position, scale, orientation).
		Internal rendering, initialize and update methods must be implemented.
	*/
	class Object
	{
	public:

		Object(const ShaderProgram& shader);
		virtual ~Object();

		void setRotation(const glm::quat& quaternion);
		void setRotation(float x, float y, float z, float angle);
		void setPosition(float x, float y, float z);
		void setScale(float x, float y, float z);

		void setShader(const ShaderProgram& shader);
		void addShader(const ShaderProgram& shader);

		glm::quat getRotation();
		glm::vec3 getPosition();
		glm::vec3 getScale();

		BoundingBox* getBoundingBox();

		const std::vector<ShaderProgram>& getShaders();

		/*
			Calculates the model matrix from the given position, scale and orientation.
		*/
		glm::mat4 getModelMatrix();

		/*
			It sets the necessary uniforms for the used shaders, then calls the internal rendering method.
		*/
		void render();

		/*
			Renders the object using a custom shader, view and projection matrix.
			Calls the internal rendering method for shadow calculation.
		*/
		void renderShadowmap(ShaderProgram& customShader, const glm::mat4& view, const glm::mat4& projection);

		/*
			Initializes the object.
		*/
		virtual void initialize(const Scene& scene) = 0;

		/*
			Updates the state of the object.
		*/
		virtual void update(const Scene& scene) = 0;
	
		void setTexture(const std::string& uniformTexture, GLuint textureId);

		/*
			TODO
		*/
		void addChild(Object* object);
		void removeChild(Object* object);

	protected:
		
		std::vector<Object> children_;
		Object* parent_;

		/*
			Texture names (uniforms used in shader) and texture ids.
			This is to dynamically assign textures to texture slots and bound them to uniforms.
		*/
		std::map<std::string, GLuint> textures_;

		BoundingBox* bounding_box_; // the original aabb in model space

		glm::vec3 position_;
		glm::vec3 scale_;
		glm::quat rotation_;

		/*
			Shaders used by this object. The first shader is the main shader.
			The uniforms computed by the global rendering functions are set for these.
			Additional uniforms can be set in the internal rendering method.
		*/
		std::vector<ShaderProgram> shaders_;

		virtual void renderInternal() = 0;
		virtual void renderShadowmapInternal() = 0;

	private:
		BoundingBox transformed_aabb_; // transformed by model_matrix, for internal use only
		static int ID_AUTO_INCR_;
		int id_;
	};
}