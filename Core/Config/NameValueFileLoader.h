#pragma once

#include <map>
#include <string>
#include <sstream>
#include <fstream>

namespace pndev
{
	template<typename N, typename V>
	class NameValueFileLoader
	{
	public:
		std::map<N, V> getNameValuePairs(const std::string& filename)
		{
			std::map<N, V> nameValue;
			std::ifstream fileStream(filename);
			if (!fileStream)
			{
				std::cout << "File " << filename << " cannot be opened for reading." << std::endl;
				return nameValue;
			}

			std::string line;
			while (std::getline(fileStream, line))
			{
				std::istringstream iss(line);
				N name;
				V value;
				if (!(iss >> name >> value)) { continue; }
				nameValue[name] = value;
			}
			return nameValue;
		}
	};
}