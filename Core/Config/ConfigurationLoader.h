#pragma once

#include <string>

#include <map>
#include <functional>
#include <memory>
#include <vector>

#include "NameValueFileLoader.h"


namespace pndev
{
	/*
		Object configuration loader.
		The specific loaders are responsible for creating objects from configuration files.
		This class caches the objects and destroys them.
	*/
	template<typename T>
	class ConfigurationLoader
	{

	public:

		~ConfigurationLoader()
		{
			for (T* obj : loaded_objects_)
			{
				delete obj;
			}
		}

		typedef std::map<std::string, std::string> ConfigLoaderInput;
		typedef std::function<T*(const ConfigLoaderInput&)> ConfigLoader;

		void registerDefaultConfiguration(const ConfigLoader& configLoader)
		{
			default_configuration_loader_ = configLoader;
		}

		void registerConfiguration(const std::string& type, const ConfigLoader& configLoader)
		{
			configuration_loaders_[type] = configLoader;
		}

		T* loadConfigurationFromFile(const std::string& filename)
		{
			ConfigLoaderInput input = name_value_loader_.getNameValuePairs(filename);
			return loadConfiguration(input);
		}
		
		T* loadConfiguration(const ConfigLoaderInput& input)
		{
			std::string type = "";
			if (input.find("type") != input.end())
			{
				type = input.at("type");
			}
			ConfigLoader& loader = default_configuration_loader_;
			if (type != "" && configuration_loaders_.find(type) != configuration_loaders_.end())
			{
				loader = configuration_loaders_[type];
			}
			T* obj = loader(input);
			if (obj != nullptr)
			{
				loaded_objects_.push_back(obj);
			}
			return obj;
		}

	private: 

		ConfigLoader default_configuration_loader_;
		std::map<std::string, ConfigLoader> configuration_loaders_;

		NameValueFileLoader<std::string, std::string> name_value_loader_;

		std::vector<T*> loaded_objects_;

	};
}