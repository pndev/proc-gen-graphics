#include "LightScriptLoader.h"

#include <sstream>
#include <iostream>

pndev::LightScriptLoader::LightScriptLoader()
{
}

pndev::LightScriptLoader::~LightScriptLoader()
{
}

pndev::DynamicLight* pndev::LightScriptLoader::operator()(const std::map<std::string, std::string>& input)
{
	glm::vec3 day, night;
	float dayLen;
	try
	{
		std::istringstream(input.at("day_length")) >> dayLen;

		std::istringstream(input.at("day.r")) >> day.r;
		std::istringstream(input.at("day.g")) >> day.g;
		std::istringstream(input.at("day.b")) >> day.b;

		std::istringstream(input.at("night.r")) >> night.r;
		std::istringstream(input.at("night.g")) >> night.g;
		std::istringstream(input.at("night.b")) >> night.b;
	}
	catch (const std::exception& e)
	{
		std::cout << "Light script configuration is not complete! (" << e.what() << ")" << std::endl;
	}
	return new 	DynamicLight(dayLen, day, night);
}