#pragma once

#include "../../SkyBox.h"
#include "../../TextureLoader.h"

namespace pndev 
{
	class SkyboxLoader
	{
	public:
		SkyboxLoader(TextureLoader& textureLoader);
		~SkyboxLoader();

		SkyBox* operator() (const std::map<std::string, std::string>& input);

	private:

		TextureLoader* const texture_loader_;
	};

}