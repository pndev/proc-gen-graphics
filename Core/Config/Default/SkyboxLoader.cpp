#include "SkyboxLoader.h"

#include <sstream>

pndev::SkyboxLoader::SkyboxLoader(TextureLoader& textureLoader) : 
	texture_loader_(&textureLoader)
{
}

pndev::SkyboxLoader::~SkyboxLoader()
{
}

pndev::SkyBox* pndev::SkyboxLoader::operator()(const std::map<std::string, std::string>& input)
{
	Texture day, night; 
	int dayLen = 0;
	try {
		day = texture_loader_->loadCubemap(
			input.at("day.px"),
			input.at("day.nx"),
			input.at("day.py"),
			input.at("day.ny"),
			input.at("day.pz"),
			input.at("day.nz")
		);

		night = texture_loader_->loadCubemap(
			input.at("night.px"),
			input.at("night.nx"),
			input.at("night.py"),
			input.at("night.ny"),
			input.at("night.pz"),
			input.at("night.nz")
		);

		std::istringstream(input.at("day_length")) >> dayLen;
	}
	catch (const std::exception& e)
	{
		std::cout << "Sky configuration is not complete!  (" << e.what() << ")" << std::endl;
	}

	ShaderProgram* shader = ShaderProgram::get("sky");

	if (shader == nullptr)
		return nullptr;

	return new SkyBox(*shader, day, night, dayLen);
}
