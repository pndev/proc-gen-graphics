#pragma once

#include "../../Camera.h"

#include <map>

namespace pndev
{
	class CameraLoader
	{
	public:
		CameraLoader();
		~CameraLoader();

		Camera* operator() (const std::map<std::string, std::string>& input);
	};

}