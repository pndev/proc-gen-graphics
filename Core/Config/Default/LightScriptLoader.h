#pragma once

#include "../../Scripts/DynamicLight.h"

#include <map>

namespace pndev
{
	class LightScriptLoader
	{
	public:
		LightScriptLoader();
		~LightScriptLoader();

		DynamicLight* operator() (const std::map<std::string, std::string>& input);

	};

}