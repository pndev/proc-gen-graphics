#pragma once

#include "../../DirectionalLight.h"

namespace pndev
{
	class LightLoader
	{
	public:
		LightLoader();
		~LightLoader();

		DirectionalLight* operator() (const std::map<std::string, std::string>& input);

	};

}