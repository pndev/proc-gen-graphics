#include "SceneLoader.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include <vector>
#include <regex>

pndev::SceneLoader::SceneLoader(
	const ConfigurationLoader<Camera>& cameraLoader,
	const ConfigurationLoader<Object>& objectLoader,
	const ConfigurationLoader<Script>& scriptLoader,
	const ConfigurationLoader<GuiElement>& guiloader,
	const ConfigurationLoader<DirectionalLight>& lightLoader,
	const ConfigurationLoader<SkyBox>& skyboxLoader) :
	camera_loader_(cameraLoader),
	object_loader_(objectLoader),
	script_loader_(scriptLoader),
	gui_loader_(guiloader),
	light_loader_(lightLoader),
	skybox_loader_(skyboxLoader)
{
}

pndev::SceneLoader::~SceneLoader()
{
}

pndev::Scene* pndev::SceneLoader::operator() (const std::map<std::string, std::string>& input)
{
	SceneSettings settings;
	std::string sceneName;
	try
	{
		std::istringstream(input.at("scene.name")) >> sceneName;
		std::istringstream(input.at("scene.shadowmap.resolution.x")) >> settings.shadowmapResolution.x;
		std::istringstream(input.at("scene.shadowmap.resolution.y")) >> settings.shadowmapResolution.y;
	}
	catch (const std::exception& e)
	{
		std::cout << "Scene configuration is not complete! (" << e.what() << ")" << std::endl;
	}

	Scene* scene = new Scene(sceneName, settings);

	std::map<int, std::map<std::string, std::string>> objects;
	std::map<int, std::map<std::string, std::string>> scripts;
	std::map<int, std::map<std::string, std::string>> cameras;
	std::map<int, std::map<std::string, std::string>> gui;
	std::map<std::string, std::string> light;
	std::map<std::string, std::string> skybox;

	std::regex nameValueRegexp("(^[a-zA-Z_]+)(?:\\[(\\d+)\\])?((?:\\.[a-zA-Z_]+)*)");
	std::smatch matches;

	std::cout << "Configuration:\n";
	for (auto nv : input)
	{
		int confIndex = 0;
		std::string name;
		std::string propname;
		if (std::regex_match(nv.first, matches, nameValueRegexp))
		{
			name = matches[1].str();
			std::string index = matches[2].str();
			propname = matches[3].str().substr(1);
			if (index != "")
			{
				std::istringstream(index) >> confIndex;
			}
		}
		else
		{
			std::cout << "Invalid name: " << nv.first << std::endl;
			continue;
		}

		std::cout << name << "[" << confIndex << "](." << propname << ")=" << nv.second << "\n";

		if (name == "object")
		{
			objects[confIndex][propname] = nv.second;
		}
		else if (name == "script")
		{
			scripts[confIndex][propname] = nv.second;

		}
		else if (name == "camera")
		{
			cameras[confIndex][propname] = nv.second;

		}
		else if (name == "gui")
		{
			gui[confIndex][propname] = nv.second;
		}
		else if (name == "light")
		{
			light[propname] = nv.second;
		}
		else if (name == "skybox")
		{
			skybox[propname] = nv.second;
		}
	}

	for (auto conf : objects)
	{
		scene->addObject(object_loader_.loadConfiguration(conf.second));
	}

	for (auto conf : scripts)
	{
		scene->addScript(script_loader_.loadConfiguration(conf.second));
	}

	for (auto conf : cameras)
	{
		scene->addCamera(camera_loader_.loadConfiguration(conf.second));
	}

	for (auto conf : gui)
	{
		scene->addGuiElement(gui_loader_.loadConfiguration(conf.second));
	}

	scene->setLight(light_loader_.loadConfiguration(light));
	scene->setSkybox(skybox_loader_.loadConfiguration(skybox));

	int activeCam = 0;
	if (input.find("scene.active_camera") != input.end()) 
	{
		std::istringstream(input.at("scene.active_camera")) >> activeCam;
	}
	scene->setActiveCamera(activeCam);

	std::cout << std::endl;

	return scene;
}
