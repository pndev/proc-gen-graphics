#include "LightLoader.h"

#include <sstream>

#include <glm\vec3.hpp>

pndev::LightLoader::LightLoader()
{
}

pndev::LightLoader::~LightLoader()
{
}

pndev::DirectionalLight* pndev::LightLoader::operator()(const std::map<std::string, std::string>& input)
{
	DirectionalLight* light = new DirectionalLight();

	glm::vec3 ambient = glm::vec3(0,0,0), diffuse = glm::vec3(0, 0, 0);
	glm::vec3 direction = glm::vec3(0, -1, 0);
	bool shadows = false;

	try 
	{
		std::istringstream(input.at("ambient.r")) >> ambient.r;
		std::istringstream(input.at("ambient.g")) >> ambient.g;
		std::istringstream(input.at("ambient.b")) >> ambient.b;

		std::istringstream(input.at("diffuse.r")) >> diffuse.r;
		std::istringstream(input.at("diffuse.g")) >> diffuse.g;
		std::istringstream(input.at("diffuse.b")) >> diffuse.b;

		std::istringstream(input.at("direction.x")) >> direction.x;
		std::istringstream(input.at("direction.y")) >> direction.y;
		std::istringstream(input.at("direction.z")) >> direction.z;

		std::istringstream(input.at("shadows")) >> shadows;
	}
	catch (const std::exception& e)
	{
		std::cout << "Light configuration is not complete! (" << e.what() << ")" << std::endl;;
	}

	light->setAmbientColor(ambient);
	light->setDiffuseColor(diffuse);
	light->setDirection(direction);	
	light->setShadows(shadows);

	return light;
}
