#pragma once

#include "../../../Procedural/FloatingIsland.h"

#include <map>

namespace pndev
{
	class TerrainLoader
	{
	public:
		TerrainLoader(TextureLoader& textureLoader);
		~TerrainLoader();

		Object* operator() (const std::map<std::string, std::string>& input);

	private:

		TextureLoader* const texture_loader_;
	};

}