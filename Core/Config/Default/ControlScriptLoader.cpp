#include "ControlScriptLoader.h"

#include <sstream>

pndev::ControlScriptLoader::ControlScriptLoader()
{
}

pndev::ControlScriptLoader::~ControlScriptLoader()
{
}

pndev::BasicControl* pndev::ControlScriptLoader::operator()(const std::map<std::string, std::string>& input)
{
	BasicControl* control = new BasicControl();

	float speed = 5;
	try 
	{
		std::istringstream(input.at("camera.speed")) >> speed;
	}
	catch (const std::exception& e)
	{
		std::cout << "Basic Control script loading exception. (" << e.what() << ")" << std::endl;
	}
	control->setCameraSpeed(speed);
	return control;
}
