#include "CameraLoader.h"

#include <sstream>
#include <iostream>

pndev::CameraLoader::CameraLoader()
{
}

pndev::CameraLoader::~CameraLoader()
{
}

pndev::Camera* pndev::CameraLoader::operator()(const std::map<std::string, std::string>& input)
{
	std::string name = "";
	float fov = 60;
	float near = 1, far = 100;

	glm::vec3 pos;
	glm::vec3 target;

	try
	{
		name = input.at("name");
		std::istringstream(input.at("fov")) >> fov;
		std::istringstream(input.at("near")) >> near;
		std::istringstream(input.at("far")) >> far;
		std::istringstream(input.at("position.x")) >> pos.x;
		std::istringstream(input.at("position.y")) >> pos.y;
		std::istringstream(input.at("position.z")) >> pos.z;
		std::istringstream(input.at("target.x")) >> target.x;
		std::istringstream(input.at("target.y")) >> target.y;
		std::istringstream(input.at("target.z")) >> target.z;
	}
	catch (const std::exception& e)
	{
		std::cout << "Camera configuration is not complete! (" << e.what() << ")"<< std::endl;
	}
	Camera* camera = new Camera(name);
	camera->setParameters(fov, near, far);
	camera->setPosition(pos);
	camera->lookAt(target);
	return camera;
}
