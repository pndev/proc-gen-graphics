#pragma once

#include <string>

#include "../../Scene.h"
#include "../ConfigurationLoader.h"
#include "../NameValueFileLoader.h"

namespace pndev
{
	class SceneLoader
	{
	public:

		SceneLoader(
			const ConfigurationLoader<Camera>& cameraLoader,
			const ConfigurationLoader<Object>& objectLoader,
			const ConfigurationLoader<Script>& scriptLoader,
			const ConfigurationLoader<GuiElement>& guiloader,
			const ConfigurationLoader<DirectionalLight>& lightLoader,
			const ConfigurationLoader<SkyBox>& skyboxLoader);

		~SceneLoader();

		Scene* operator() (const std::map<std::string, std::string>& input);

	private:

		ConfigurationLoader<Camera> camera_loader_;
		ConfigurationLoader<Object> object_loader_;
		ConfigurationLoader<Script> script_loader_;
		ConfigurationLoader<GuiElement> gui_loader_;
		ConfigurationLoader<DirectionalLight> light_loader_;
		ConfigurationLoader<SkyBox> skybox_loader_;

	};
}