#include "TerrainLoader.h"

pndev::TerrainLoader::TerrainLoader(TextureLoader& textureLoader) :
	texture_loader_(&textureLoader)
{
}

pndev::TerrainLoader::~TerrainLoader()
{
}

pndev::Object* pndev::TerrainLoader::operator()(const std::map<std::string, std::string>& input)
{
	IslandGenerationSettings islandGenerationSettings;
	GrassSettings grassSettings;
	IslandSettings islandSettings;
	int seed;

	try {
		std::istringstream(input.at("sides")) >> islandGenerationSettings.sides;
		std::istringstream(input.at("scale_fallback")) >> islandGenerationSettings.scale_fallback;
		std::istringstream(input.at("base_radius")) >> islandGenerationSettings.base_radius;
		std::istringstream(input.at("max_y")) >> islandGenerationSettings.max_y;
		std::istringstream(input.at("min_y")) >> islandGenerationSettings.min_y;
		std::istringstream(input.at("x_resolution")) >> islandGenerationSettings.x_resolution;
		std::istringstream(input.at("y_resolution")) >> islandGenerationSettings.y_resolution;
		std::istringstream(input.at("noise_fallback")) >> islandGenerationSettings.noise_fallback;
		std::istringstream(input.at("noise_octaves")) >> islandGenerationSettings.noise_octaves;
		std::istringstream(input.at("noise_scale")) >> islandGenerationSettings.noise_scale;

		std::istringstream(input.at("grass_density")) >> grassSettings.grass_density;
		std::istringstream(input.at("max_slope_angle")) >> grassSettings.max_slope_angle;

		std::istringstream(input.at("float_elevation")) >> islandSettings.float_elevation;
		std::istringstream(input.at("float_velocity")) >> islandSettings.float_velocity;
		std::istringstream(input.at("max_tilt_angle")) >> islandSettings.max_tilt_angle;
		islandSettings.grass = texture_loader_->loadTexture(input.at("grass_texture"));
		islandSettings.rock = texture_loader_->loadTexture(input.at("rock_texture"));
		islandSettings.grass_blades = texture_loader_->loadTexture(input.at("grass_blades"), 4, false);
		std::istringstream(input.at("grass_inverse_scale")) >> islandSettings.grass_inverse_scale;
		std::istringstream(input.at("rock_inverse_scale")) >> islandSettings.rock_inverse_scale;
		std::istringstream(input.at("wind_speed")) >> islandSettings.wind_speed;
		std::istringstream(input.at("wind_direction.x")) >> islandSettings.wind_direction.x;
		std::istringstream(input.at("wind_direction.y")) >> islandSettings.wind_direction.y;
		std::istringstream(input.at("wind_direction.z")) >> islandSettings.wind_direction.z;
		std::istringstream(input.at("base_position.x")) >> islandSettings.base_position.x;
		std::istringstream(input.at("base_position.y")) >> islandSettings.base_position.y;
		std::istringstream(input.at("base_position.z")) >> islandSettings.base_position.z;
		std::istringstream(input.at("grass_distance")) >> islandSettings.grass_distance;
		std::istringstream(input.at("max_grass_instances")) >> islandSettings.max_grass_instances;
		std::istringstream(input.at("seed")) >> seed;
	}
	catch (const std::exception& e)
	{
		std::cout << "Terrain configuration is not complete! (" << e.what() << ")" << std::endl;
	}
	ShaderProgram* terrainShader = ShaderProgram::get("terrain");
	ShaderProgram* grassShader = ShaderProgram::get("grass");

	FloatingIsland* floatingIslang = nullptr;

	IslandGenerator generator(islandGenerationSettings);
	GrassGenerator grassGenerator(grassSettings);

	if (terrainShader != nullptr && grassShader != nullptr)
		floatingIslang = new FloatingIsland(seed, islandSettings, generator, grassGenerator, *terrainShader, *grassShader);

	return floatingIslang;
}
