#pragma once

#include "../../Scripts/BasicControl.h"

namespace pndev
{
	class ControlScriptLoader
	{
	public:
		ControlScriptLoader();
		~ControlScriptLoader();

		BasicControl* operator() (const std::map<std::string, std::string>& input);
	};

}