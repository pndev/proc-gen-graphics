#include "Object.h"

#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\quaternion.hpp>

int pndev::Object::ID_AUTO_INCR_ = 0;

pndev::Object::Object(const ShaderProgram& shader) :
	id_(ID_AUTO_INCR_++),
	position_(glm::vec3(0, 0, 0)),
	rotation_(glm::quat()),
	scale_(glm::vec3(1, 1, 1))

{
	shaders_.push_back(shader);
}

pndev::Object::~Object()
{
}

void pndev::Object::setRotation(const glm::quat& quaternion)
{
	rotation_ = quaternion;
	rotation_ = glm::normalize(rotation_);
};

void pndev::Object::setRotation(float x, float y, float z, float angle)
{
	rotation_ = glm::angleAxis(glm::radians(angle), glm::vec3(x, y, z));
};

void pndev::Object::setPosition(float x, float y, float z)
{
	position_.x = x;
	position_.y = y;
	position_.z = z;
};

void pndev::Object::setScale(float x, float y, float z)
{
	scale_.x = x;
	scale_.y = y;
	scale_.z = z;
}
void pndev::Object::setShader(const ShaderProgram& shader)
{
	shaders_[0] = shader;
}

void pndev::Object::addShader(const ShaderProgram& shader)
{
	shaders_.push_back(shader);
}

glm::quat pndev::Object::getRotation()
{
	return rotation_;
};

glm::vec3 pndev::Object::getPosition()
{
	return position_;
}

glm::vec3 pndev::Object::getScale()
{
	return scale_;
}

pndev::BoundingBox* pndev::Object::getBoundingBox()
{
	if (bounding_box_ == nullptr) return nullptr;
	transformed_aabb_ = bounding_box_->getTransformedByModelMatrix(getModelMatrix());
	return &transformed_aabb_;
}

const std::vector<pndev::ShaderProgram>& pndev::Object::getShaders()
{
	return shaders_;
}

glm::mat4 pndev::Object::getModelMatrix()
{
	glm::mat4 S = glm::scale(glm::mat4(), scale_);
	S[3][3] = 1;
	glm::mat4 R = glm::toMat4(rotation_);
	glm::mat4 invR = glm::transpose(R);
	glm::vec4 t = glm::vec4(position_.x, position_.y, position_.z, 1)*invR;
	glm::vec3 t3(t.x, t.y, t.z);
	return glm::translate(R*S, t3);
};

void pndev::Object::render()
{
	if (shaders_.size() != 0)
	{
		for (auto shader : shaders_) 
		{
			shader.setProperty("model_matrix", getModelMatrix());

			int textureId = 0;
			for (std::pair<std::string, GLuint> uniformToTexture : textures_)
			{
				shader.setProperty(uniformToTexture.first, textureId);
				glActiveTexture(GL_TEXTURE0 + textureId);
				glBindTexture(GL_TEXTURE_2D, uniformToTexture.second);
				textureId++;
			}
		}
	}

	renderInternal();
}

void pndev::Object::renderShadowmap(ShaderProgram& customShader, const glm::mat4& view, const glm::mat4& projection)
{
	customShader.setProperty("model_matrix", getModelMatrix());
	customShader.setProperty("view_matrix", view);
	customShader.setProperty("projection_matrix", projection);

	renderShadowmapInternal();
}

void pndev::Object::setTexture(const std::string& uniformTexture, GLuint textureId)
{
	textures_[uniformTexture] = textureId;
}

