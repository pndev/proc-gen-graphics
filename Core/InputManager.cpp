#include "InputManager.h"

#include <GLFW\glfw3.h>

pndev::InputManager::InputManager(GLFWwindow * window) :
	window_(window),
	callback_id_(0)
{
	mouse_data_.left_pressed = false;	
	mouse_data_.right_pressed = false;
	mouse_data_.delta_x = 0;
	mouse_data_.delta_y = 0;
	mouse_data_.position_x = 0;
	mouse_data_.position_y = 0;
}

void pndev::InputManager::update()
{
	glfwPollEvents();

	double cx, cy;
	glfwGetCursorPos(window_, &cx, &cy);
	mouse_data_.delta_x = cx - mouse_data_.position_x;
	mouse_data_.delta_y = cy - mouse_data_.position_y;
	mouse_data_.position_x = cx;
	mouse_data_.position_y = cy;
}

void pndev::InputManager::keyCallback(int key, int scancode, int action, int mods)
{
	for (auto callback : key_callback_)
	{
		KeyCallback keyfunct = callback.second;
		keyfunct(key, scancode, action, mods);
	}
}

void pndev::InputManager::mouseCallback(int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		mouse_data_.right_pressed = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		mouse_data_.right_pressed = false;
	}
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		mouse_data_.left_pressed = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		mouse_data_.left_pressed = false;
	}

	for (auto callback : mouse_button_callback_)
	{
		MouseButtonCallback funct = callback.second;
		funct(button, action, mods);
	}
}

int pndev::InputManager::registerKeyCallback(const KeyCallback& callback)
{
	key_callback_[callback_id_++] = callback;
	return callback_id_ - 1;
}

void pndev::InputManager::unregisterKeyCallback(int id)
{
	key_callback_.erase(id);
}

int pndev::InputManager::registerMouseCallback(const MouseButtonCallback& callback)
{
	mouse_button_callback_[callback_id_++] = callback;
	return callback_id_ - 1;
}

void pndev::InputManager::unregisterMouseCallback(int id)
{
	mouse_button_callback_.erase(id);
}

const pndev::MouseData& pndev::InputManager::getMouseInfo()
{
	return mouse_data_;
}
