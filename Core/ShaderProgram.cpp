#include "ShaderProgram.h"

#include <vector>

#include <glm\gtc\type_ptr.hpp>

int pndev::operator+ (pndev::ShaderType val)
{
	return static_cast<int>(val);
}

std::map<std::string, pndev::ShaderProgram> pndev::ShaderProgram::shader_cache_;

pndev::ShaderProgram* pndev::ShaderProgram::get(std::string name)
{
	if (shader_cache_.find(name) != shader_cache_.end())
		return &shader_cache_.at(name);
	return nullptr;
}

pndev::ShaderProgram::ShaderProgram(const std::string & programName, const std::string & vertexShaderFile, const std::string & fragmentShaderFile) :
	ShaderProgram(
		programName,
		vertexShaderFile,
		fragmentShaderFile,
		nullptr, nullptr, nullptr)
{
}

pndev::ShaderProgram::ShaderProgram(
	const std::string& programName,
	const std::string& vertexShaderFile,
	const std::string& fragmentShaderFile,
	const char* geometryShader,
	const char* tessControlShader,
	const char* tessEvShader) :
	id_(programName),
	program_(GL_NONE)
{
	std::fill_n(shaders_, sizeof(shaders_) / sizeof(*shaders_), 0);

	load(vertexShaderFile, fragmentShaderFile, tessControlShader, tessEvShader);

	shader_cache_.insert(std::make_pair(programName, *this));
}

pndev::ShaderProgram::~ShaderProgram()
{
	if (program_.use_count() == 1) 
	{
		GLuint program = *program_.get();
		if (*program_.get() != GL_NONE)
		{
			glDeleteProgram(program);
		}

		for (int i = 0; i < sizeof(shaders_) / sizeof(*shaders_); i++)
		{
			if (shaders_[i] != 0) glDeleteShader(shaders_[i]);
		}
	}
};

void pndev::ShaderProgram::load(
	const std::string & vertexShaderFile,
	const std::string & fragmentShaderFile,
	const char * tessControlShaderFile,
	const char * tessEvShaderFile)
{
	std::string vss = loadFile(vertexShaderFile);
	std::string fss = loadFile(fragmentShaderFile);
	std::string tess;
	std::string tcss;

	const char* vertexShaderSource = vss.c_str();
	const char* fragmentShaderSource = fss.c_str();
	const char* tessControlShaderSource = nullptr;
	const char* tessEvShaderSource = nullptr;

	if (tessControlShaderFile != nullptr && tessEvShaderFile != nullptr)
	{
		tess = loadFile(std::string(tessControlShaderFile));
		tcss = loadFile(std::string(tessEvShaderFile));
		tessControlShaderSource = tess.c_str();
		tessEvShaderSource = tcss.c_str();
	};

	GLuint program = glCreateProgram();

	shaders_[+ShaderType::VERTEX] = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(shaders_[+ShaderType::VERTEX], 1, &vertexShaderSource, NULL);
	glCompileShader(shaders_[+ShaderType::VERTEX]);
	if (getCompilationStatus(shaders_[+ShaderType::VERTEX]) != 0)
	{
		glDeleteShader(shaders_[+ShaderType::VERTEX]);
		shaders_[+ShaderType::VERTEX] = 0;
		return;
	}

	shaders_[+ShaderType::FRAGMENT] = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(shaders_[+ShaderType::FRAGMENT], 1, &fragmentShaderSource, NULL);
	glCompileShader(shaders_[+ShaderType::FRAGMENT]);
	if (getCompilationStatus(shaders_[+ShaderType::FRAGMENT]) != 0)
	{
		glDeleteShader(shaders_[+ShaderType::FRAGMENT]);
		shaders_[+ShaderType::FRAGMENT] = 0;
		return;
	}

	if (tessControlShaderSource != nullptr && tessEvShaderSource != nullptr)
	{
		shaders_[+ShaderType::TESSCONT] = glCreateShader(GL_TESS_CONTROL_SHADER);
		glShaderSource(shaders_[+ShaderType::TESSCONT], 1, &tessControlShaderSource, NULL);
		glCompileShader(shaders_[+ShaderType::FRAGMENT]);
		if (getCompilationStatus(shaders_[+ShaderType::TESSCONT]) != 0)
		{
			glDeleteShader(shaders_[+ShaderType::TESSCONT]);
			shaders_[+ShaderType::TESSCONT] = 0;
			return;
		}

		shaders_[+ShaderType::TESSEV] = glCreateShader(GL_TESS_EVALUATION_SHADER);
		glShaderSource(shaders_[+ShaderType::TESSEV], 1, &tessEvShaderSource, NULL);
		glCompileShader(shaders_[+ShaderType::TESSEV]);
		if (getCompilationStatus(shaders_[+ShaderType::TESSEV]) != 0)
		{
			glDeleteShader(shaders_[+ShaderType::TESSEV]);
			shaders_[+ShaderType::TESSEV] = 0;
			return;
		}
	}

	for (int i = 0; i < sizeof(shaders_) / sizeof(*shaders_); i++)
	{
		if (shaders_[i] != 0) glAttachShader(program, shaders_[i]);
	}

	glLinkProgram(program);
	
	int isLinked;
	glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
	
	GLint maxLength = 0;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 0) 
	{
		std::vector<GLchar> log(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &log[0]);

		std::cout << "Program linking log:\n" << std::string(log.data(), maxLength) << "\n";
	}

	if (isLinked == GL_FALSE)
	{
		glDeleteProgram(program);
		program = GL_NONE;
	}

	program_ = std::make_shared<GLuint>(program);
};

void pndev::ShaderProgram::use()
{
	glUseProgram(*program_);
};

GLuint pndev::ShaderProgram::getProgram() { return *program_; }

bool pndev::ShaderProgram::isLoaded() { return *program_ != GL_NONE; }

std::string pndev::ShaderProgram::loadFile(const std::string & path)
{
	std::ifstream t(path);

	if (t)
	{
		std::cout << "Reading file " << path << "\n";
	}
	else
	{
		std::cout << "Error reading file " << path << "\n";
	}

	std::string str((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());
	return str;
}

int pndev::ShaderProgram::getCompilationStatus(GLuint shader)
{
	GLint isCompiled = 0;

	GLint maxLength = 0;

	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	if (maxLength > 0)
	{
		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(shader, maxLength, &maxLength, &infoLog[0]);
		std::cout << "Shader info:\n" << std::string(infoLog.data(), maxLength) << "\n";
	}

	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		return -1;
	}

	return 0;
}

void pndev::ShaderProgram::useNone()
{
	glUseProgram(GL_NONE);
}

GLuint pndev::ShaderProgram::getUniformLocation(const std::string & uniform)
{
	return glGetUniformLocation(*program_, uniform.c_str());
}

bool pndev::ShaderProgram::setProperty(std::string prop, int val)
{
	GLuint loc = getUniformLocation(prop);
	if (loc == -1) return false;
	use();
	glUniform1i(loc, val);
	return true;
}

bool pndev::ShaderProgram::setProperty(std::string prop, float val)
{
	GLuint loc = getUniformLocation(prop);
	if (loc == -1) return false;
	use();
	glUniform1f(loc, val);
	return true;
}

bool pndev::ShaderProgram::setProperty(std::string prop, const glm::vec2& val)
{
	GLuint loc = getUniformLocation(prop);
	if (loc == -1) return false;
	use();
	glUniform2f(loc, val.x, val.y);
	return true;
}

bool pndev::ShaderProgram::setProperty(std::string prop, const glm::vec3& val)
{
	GLuint loc = getUniformLocation(prop);
	if (loc == -1) return false;
	use();
	glUniform3f(loc, val.x, val.y, val.z);
	return true;
}

bool pndev::ShaderProgram::setProperty(std::string prop, const glm::vec4& val)
{
	GLuint loc = getUniformLocation(prop);
	if (loc == -1) return false;
	use();
	glUniform4f(loc, val.x, val.y, val.z, val.w);
	return true;
}

bool pndev::ShaderProgram::setProperty(std::string prop, const glm::mat4& val)
{
	GLuint loc = getUniformLocation(prop);
	if (loc == -1) return false;
	use();
	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(val));
	return true;
}
