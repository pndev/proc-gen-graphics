#pragma once

#include "GL.h"

#include <functional>
#include <map>

namespace pndev
{
	struct MouseData
	{
		double delta_x;
		double delta_y;
		double position_x;
		double position_y;
		bool left_pressed;
		bool right_pressed;
	};

	class InputManager
	{
		typedef std::function<void(int, int, int, int)> KeyCallback;
		typedef std::function<void(int, int, int)> MouseButtonCallback;

	public:

		InputManager(GLFWwindow* window);

		void update();

		void keyCallback(int key, int scancode, int action, int mods);
		void mouseCallback(int button, int action, int mods);

		/*
			int key, int scancode, int action, int mods
		*/
		int registerKeyCallback(const KeyCallback& callback);
		void unregisterKeyCallback(int id);

		/*
			int button, int action, int mods
		*/
		int registerMouseCallback(const MouseButtonCallback& callback);
		void unregisterMouseCallback(int id);

		const MouseData& getMouseInfo();

	private:

		MouseData mouse_data_;

		GLFWwindow* window_;

		int callback_id_;
		std::map<int, KeyCallback> key_callback_;
		std::map<int, MouseButtonCallback> mouse_button_callback_;
	};
}