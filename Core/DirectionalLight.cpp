#include "DirectionalLight.h"

#include "GL.h"

pndev::DirectionalLight::DirectionalLight():
	has_shadows_(true)
{

}


pndev::DirectionalLight::~DirectionalLight()
{

}

void pndev::DirectionalLight::setShadows(bool sh)
{
	has_shadows_ = sh;
}

void pndev::DirectionalLight::setDirection(const glm::vec3& dir)
{
	direction_ = dir;
}

void pndev::DirectionalLight::setDiffuseColor(const glm::vec3& color)
{
	diffuse_color_ = color;
}

void pndev::DirectionalLight::setAmbientColor(const glm::vec3& color)
{
	ambient_color_ = color;
}

glm::vec3 pndev::DirectionalLight::getDirection() const
{
	return direction_;
}

glm::vec3 pndev::DirectionalLight::getDiffuseColor() const
{
	return diffuse_color_;
}

glm::vec3 pndev::DirectionalLight::getAmbientColor() const
{
	return ambient_color_;
}

bool pndev::DirectionalLight::castShadows() const
{
	return has_shadows_;
}
