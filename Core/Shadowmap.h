#pragma once

#include "GL.h"

#include "Object.h"
#include "TextureFilter.h"

#include <vector>

namespace pndev
{
	class Shadowmap
	{
	public:
		Shadowmap(int width, int height);
		~Shadowmap();

		void computeShadowmap(const glm::vec3& lightDirection, const std::vector<Object*>& objects);

		GLuint getDepthTexture();
		glm::mat4 getLigthMatrix();

	private:

		void initFbo();

		ShaderProgram shadowmap_shader_;
		ShaderProgram blur_shader_;

		TextureFilter* blur_filter_;

		GLuint filtered_depth_texture_;
		GLuint depth_texture_;
		GLuint shadow_fbo_;

		glm::mat4 light_matrix_;

		int width_;
		int height_;
	};

}