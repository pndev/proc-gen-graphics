#pragma once

#include "Scene.h"

#include <vector>
#include <functional>
#include <map>

#include "Config\ConfigurationLoader.h"

namespace pndev
{
	struct ContextSettings
	{
		static const int MIN_SUPPORTED_MAJOR_VERSION = 4;
		static const int MIN_SUPPORTED_MINOR_VERSION = 0;

		std::string window_title;
		int window_width;
		int window_height;
		int opengl_major_version;
		int opengl_minor_version;

		std::string resource_path;

		ContextSettings()
		{
			window_title = "OpenGL Application";
			window_width = 1024;
			window_height = 768;
			opengl_major_version = MIN_SUPPORTED_MAJOR_VERSION;
			opengl_minor_version = MIN_SUPPORTED_MINOR_VERSION;
			resource_path = "./";
		}
	};

	class Engine
	{
	public:


		Engine(const ContextSettings& settings);

		~Engine();

		void run();

		Scene* loadScene(const std::string& sceneConfig);
		void makeCurrent(const std::string& name);

	private:

		/*
			The active engine object.
			It is used to reference internal configuration from static methods (mainly GLFW callbacks).
		*/
		static Engine* engine_;

		ContextSettings settings_;
		GLFWwindow* window_;

		static void errorCallback(int error, const char* description);
		static void framebufferSizeCallback(GLFWwindow* window, int width, int height);
		static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
		static void fileDropcallback(GLFWwindow* window, int files, const char** filenames);

		Scene* current_scene_;
		std::vector<Scene*> scenes_;

		InputManager* input_manager_;

		TextureLoader texture_loader_;

		ConfigurationLoader<Scene> scene_loader_; 
		ConfigurationLoader<Camera> camera_loader_;
		ConfigurationLoader<Object> object_loader_;
		ConfigurationLoader<Script> script_loader_;
		ConfigurationLoader<GuiElement> gui_loader_;
		ConfigurationLoader<DirectionalLight> light_loader_;
		ConfigurationLoader<SkyBox> skybox_loader_;

		std::vector<Texture> loaded_textures_;
	};
}