#pragma once

#include "GL.h"

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include <memory>

#include <glm\glm.hpp>

namespace pndev 
{
	enum class ShaderType : unsigned int
	{
		VERTEX,
		FRAGMENT,
		TESSCONT,
		TESSEV,
		GEOMETRY
	};

	int operator+ (ShaderType val);

	/*
		Immutable, copyable
	*/
	class ShaderProgram
	{
	public:

		static ShaderProgram* get(std::string name);

		ShaderProgram(
			const std::string& programName,
			const std::string& vertexShaderFile,
			const std::string& fragmentShaderFile);

		ShaderProgram(
			const std::string& programName,
			const std::string& vertexShaderFile,
			const std::string& fragmentShaderFile,
			const char* geometryShader, // still not used
			const char* tessControlShader,
			const char* tessEvShader);

		~ShaderProgram();

		/*
			Loads the shaders using the file paths given and builds a program object.
			Tessellation shaders are not necessary.
		*/

		void use();
		static void useNone();

		GLuint getUniformLocation(const std::string& uniform);

		/*
			Common uniform properties.
		*/
		bool setProperty(std::string prop, const int val);
		bool setProperty(std::string prop, const float val);
		bool setProperty(std::string prop, const glm::vec2& val);
		bool setProperty(std::string prop, const glm::vec3& val);
		bool setProperty(std::string prop, const glm::vec4& val);
		bool setProperty(std::string prop, const glm::mat4& val);

		GLuint getProgram();
		bool isLoaded();

	private:

		std::string id_;
		std::shared_ptr<GLuint> program_;
		GLuint shaders_[5];

		static std::map<std::string, ShaderProgram> shader_cache_;

		void load(
			const std::string& vertexShaderFile,
			const std::string& fragmentShaderFile,
			const char* tessControlShader,
			const char* tessEvShader);

		static std::string loadFile(const std::string& path);
		static int getCompilationStatus(GLuint shader);
	};


}