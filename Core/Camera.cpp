#include "Camera.h"

static float EPSILON = 0.0001f;

pndev::Camera::Camera():
	Camera("")
{
}

pndev::Camera::Camera(const std::string& name) :
	name_(name),
	position_(0, 0, 0),
	forward_(0, 0, 1),
	up_(0, 1, 0),
	fov_(60),
	near_(1),
	far_(10),
	width_(1024),
	height_(1024)
{
};


pndev::Camera::~Camera()
{

}; 

void pndev::Camera::update(int width, int height)
{
	width_ = width;
	height_ = height;
};

glm::mat4 pndev::Camera::getViewMatrix() const
{
	return glm::lookAt(position_, position_ + forward_, up_);
};

glm::mat4 pndev::Camera::getProjectionMatrix() const
{
	return glm::perspective(glm::radians(fov_), (float)width_ / height_, near_, far_);
}; 

void pndev::Camera::getViewPort(int& width, int& height) const
{
	height = height_;
	width = width_;
}; 

void pndev::Camera::getParameters(float& fov, float& near, float& far) const
{
	fov = this->fov_;
	near = this->near_;
	far = this->far_;
};

void pndev::Camera::setParameters(float fov, float near, float far)
{
	fov_ = fov;
	near_ = near;
	far_ = far;
}

glm::vec3 pndev::Camera::getPosition() const
{
	return position_;
}

glm::vec3 pndev::Camera::getForward() const
{
	return forward_;
}

glm::vec3 pndev::Camera::getRight() const
{
	return glm::normalize(glm::cross(forward_, up_));
}

void pndev::Camera::setPosition(const glm::vec3& position)
{
	position_ = position;
}

void pndev::Camera::lookAt(const glm::vec3& target)
{
	glm::vec3 forw = target - position_;
	if (glm::length(forw) > EPSILON)
	{
		up_ = glm::vec3(0.0f, 1.0f, 0.0f);
		forward_ = glm::normalize(forw);
	}
}

void pndev::Camera::rotateByDelta(float horizontal, float vertical)
{
	glm::vec3 right = getRight();

	glm::mat4 hrm = glm::rotate(glm::mat4(), horizontal, glm::vec3(0, 1.0f, 0));
	glm::mat4 vrm = glm::rotate(glm::mat4(), vertical, right);

	forward_ = vrm*hrm*glm::vec4(forward_, 1);
	right = hrm*glm::vec4(right, 1);
	right.y = 0; // important because of float error propagation
	up_ = glm::normalize(glm::cross(right, forward_));
}