#version 400

#define PI 3.1415f

in vec3 textureCoord;
out vec4 color;

struct light_data
{
	vec3 direction;
	vec3 diffuse;
	vec3 ambient;
};

uniform samplerCube day_texture;
uniform samplerCube night_texture;
uniform light_data directional_light;
uniform float day_time;
uniform float day_length;

void main()
{   
	vec3 tx = normalize(textureCoord);

	vec3 ldnorm = -normalize(directional_light.direction);

	float dayToNight = clamp(1.0f - ldnorm.y, 0.0f, 1.0f);

    vec4 dayColor = texture(day_texture, tx);
    vec4 nightColor = texture(night_texture, tx);

	float c = max(dot(ldnorm, tx), 0.00001f);
	float distance = sqrt(1.0f-c*c)/c;
	float light = clamp(pow(max(1.05f - distance, 0.0f), 64), 0.0f, 1.0f);
	
	float lightDistToHorizont = abs(ldnorm.y);
	float pixelDistToHorizont = abs(tx.y);

	float scatteringLightCoeff = pow(max(1.0f - lightDistToHorizont, 0.0f), 2);
	float scatteringPixelCoeff = pow(max(1.0f - pixelDistToHorizont, 0.0f), 4) * pow(2, -distance);

	float falloffDistance = abs(min(0.0f, tx.y));
	float falloffCoeff = clamp(pow(max(1.05f - falloffDistance, 0.0f), 64) - 0.05f, 0.0f, 1.0f);

	float dt = dayToNight;
	float dirLightDisk = clamp(falloffCoeff*light + scatteringPixelCoeff*scatteringLightCoeff, 0.0f, 1.0f);
	color = mix(mix(dayColor, nightColor, dt), vec4(1.5f*directional_light.diffuse, 1.0f), dirLightDisk);
}