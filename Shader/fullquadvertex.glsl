#version 400
 
layout(location = 0) in vec2 vpos;
 
out vec2 uv;

void main()
{
	uv = (vpos + 1) / 2;
	gl_Position = vec4(vpos.x, vpos.y, 0, 1);
}