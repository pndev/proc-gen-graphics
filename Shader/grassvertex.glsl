#version 400

#define PI 3.141592f

layout(location=0) in vec3 local_position;
layout(location=1) in vec2 texture_coord;
layout(location=2) in vec3 instance_position;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

uniform float time;
uniform float wind_speed;
uniform vec3 wind_direction;

uniform mat4 light_vp;

out vec2 uv;
out vec3 normal;
out vec4 lightspacepos;

float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{
	mat4 normalMatrix = transpose(inverse(model_matrix));
	normal = normalize(vec3(normalMatrix * vec4(0,1,0,1)));
	uv = texture_coord;
	vec3 lpos = local_position;

	float randg = rand(instance_position.xz);

	vec3 windDirWithNoise = normalize(wind_direction)+vec3(sin(randg),0,cos(randg));

	lpos.xz = lpos.xz + 0.5f * lpos.y * sin(wind_speed * (sin(randg) + 1)/2 * (time + cos(randg)) * PI * 2) * normalize(windDirWithNoise.xz);
	lightspacepos =  light_vp * model_matrix * vec4(lpos + instance_position, 1.0f);
	gl_Position = projection_matrix*view_matrix*model_matrix*vec4(lpos + instance_position, 1.0f);
}