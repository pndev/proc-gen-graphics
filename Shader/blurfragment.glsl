#version 400

out vec3 color;

in vec2 uv;

uniform sampler2D original_texture;
uniform int texture_width;
uniform int texture_height;

vec4 blur13(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
  vec4 color = vec4(0.0);
  vec2 off1 = vec2(1.411764705882353) * direction;
  vec2 off2 = vec2(3.2941176470588234) * direction;
  vec2 off3 = vec2(5.176470588235294) * direction;
  color += texture2D(image, uv) * 0.1964825501511404;
  color += texture2D(image, uv + (off1 / resolution)) * 0.2969069646728344;
  color += texture2D(image, uv - (off1 / resolution)) * 0.2969069646728344;
  color += texture2D(image, uv + (off2 / resolution)) * 0.09447039785044732;
  color += texture2D(image, uv - (off2 / resolution)) * 0.09447039785044732;
  color += texture2D(image, uv + (off3 / resolution)) * 0.010381362401148057;
  color += texture2D(image, uv - (off3 / resolution)) * 0.010381362401148057;
  return color;
}

void main()
{
	vec3 a =  blur13(original_texture, uv, vec2(texture_width, texture_height), vec2(1, 0)).rgb;
	vec3 b =  blur13(original_texture, uv, vec2(texture_width, texture_height), vec2(0, 1)).rgb;
	color = (a+b)/2;
}