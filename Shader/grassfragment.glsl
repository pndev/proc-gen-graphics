#version 400

struct light_data
{
	vec3 direction;
	vec3 diffuse;
	vec3 ambient;
};

in vec4 lightspacepos;
in vec3 normal;
in vec2 uv;
out vec4 color;

uniform sampler2D grass_blade_texture;
uniform sampler2D depth_texture;
uniform light_data directional_light;

float linstep(float min, float max, float v)  
{  
  return clamp((v - min) / (max - min), 0, 1);  
}

float reduceLightBleed(float p_max, float a)  
{
   return linstep(a, 1, p_max);  
}  

float chebyshev(vec2 moments, float t)
{
	float p = (t <= moments.x) ? 1 : 0;
	float variance = moments.y - moments.x*moments.x;
	variance = max(variance, 0.0005f);
	float d = t - moments.x;
	float p_max = variance / (variance + d*d);
	return max(p, reduceLightBleed(p_max, 0.4f));
}

float calcShadows(vec4 fpos)
{
    vec3 pcoords = fpos.xyz / fpos.w * 0.5f + 0.5f;
    float currdepth = pcoords.z;
	return chebyshev(texture(depth_texture, pcoords.xy).xy, currdepth);
}

void main()
{
	vec3 lightdir = normalize(directional_light.direction);
	float lndot = dot(normalize(normal), -lightdir);
	float shadow = calcShadows(lightspacepos);
	if (lndot <= 0) shadow = 1;
	
	float diffuse = mix(clamp(lndot, 0.0f, 1.0f), 0.0f, clamp(lightdir.y/0.05f, 0.0f, 1.0f));

	vec3 diff = shadow*diffuse*directional_light.diffuse + directional_light.ambient;

	vec4 tcolor = texture(grass_blade_texture, uv); 
	if(tcolor.a < 0.1f) { discard; }
	color = vec4(diff*tcolor.rgb, tcolor.a);
}