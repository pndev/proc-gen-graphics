#version 400

struct light_data
{
	vec3 direction;
	vec3 diffuse;
	vec3 ambient;
};

uniform light_data directional_light;
uniform sampler2D depth_texture;

uniform float grass_scale;
uniform float rock_scale;

uniform sampler2D grass_texture;
uniform sampler2D rock_texture;

out vec3 color;

in vec3 local_position;
in vec3 local_normal;
in vec3 normal;
in vec4 lightspacepos;
   
float linstep(float min, float max, float v)  
{  
  return clamp((v - min) / (max - min), 0, 1);  
}

float reduceLightBleed(float p_max, float a)  
{
   return linstep(a, 1, p_max);  
}  

float chebyshev(vec2 moments, float t)
{
	float p = (t <= moments.x) ? 1 : 0;
	float variance = moments.y - moments.x*moments.x;
	variance = max(variance, 0.0005f);
	float d = t - moments.x;
	float p_max = variance / (variance + d*d);
	return max(p, reduceLightBleed(p_max, 0.4f));
}

float calcShadows(vec4 fpos)
{
    vec3 pcoords = fpos.xyz / fpos.w * 0.5f + 0.5f;
    float currdepth = pcoords.z;
	return chebyshev(texture(depth_texture, pcoords.xy).xy, currdepth);
}

void main()
{
	vec3 normaln = normalize(normal);
	vec3 lnorm = normalize(local_normal);
	vec3 lightdir = normalize(directional_light.direction);
	float lndot = dot(normaln, -lightdir);
	float shadow = calcShadows(lightspacepos);
	if (lndot <= 0) shadow = 1;

	vec3 blending = abs(lnorm);
	blending = normalize(max(blending, 0.00001));
	float b = (blending.x + blending.y + blending.z);
	blending /= vec3(b, b, b);

	float diffuse = mix(clamp(lndot, 0.0f, 1.0f), 0.0f, clamp(lightdir.y/0.05f, 0.0f, 1.0f));

	vec3 grassxz = texture(grass_texture, local_position.xz*grass_scale).rgb;
	vec3 rockxz = texture(rock_texture, local_position.xz*rock_scale).rgb;

	vec3 grassxy = texture(grass_texture, local_position.xy*grass_scale).rgb;
	vec3 rockxy = texture(rock_texture, local_position.xy*rock_scale).rgb;

	vec3 grassyz = texture(grass_texture, local_position.yz*grass_scale).rgb;
	vec3 rockyz = texture(rock_texture, local_position.yz*rock_scale).rgb;

	vec3 grass = grassyz * blending.x + grassxz * blending.y + grassxy * blending.z;
	vec3 rock = rockyz * blending.x + rockxz * blending.y + rockxy * blending.z;

	//grass = mix(vec3(0.1, 0.3, 0.1), grass, linstep(0.8f, 1.0f, lnorm.y));
	vec3 textureColor = mix(rock, grass, linstep(0.5f, 0.8f, lnorm.y));

	color = (shadow*diffuse*directional_light.diffuse + directional_light.ambient) * textureColor;
	//color = texture(depth_texture,fract(local_position.xz/10)).rrr;
	//color = vec3(0,0,0);	
	//color = vec3(1,0,1);
	//color = vec3(1,1,1);
}
