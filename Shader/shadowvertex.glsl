#version 400
 
layout(location = 0) in vec3 vpos;
 
uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;
 
 out vec2 position;

void main()
{
	vec4 pos = projection_matrix * view_matrix * model_matrix * vec4(vpos, 1.0);
	position = pos.zw;
    gl_Position = pos;
}
