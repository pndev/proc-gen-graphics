#version 400
 
layout(location = 0) in vec3 vpos;
layout(location = 1) in vec3 norm;
 
uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;
uniform mat4 light_vp;

out vec3 local_position;
out vec3 local_normal;
out vec3 normal;
out vec4 lightspacepos;
 
void main()
{
	local_normal = norm;
	mat4 normalMatrix = transpose(inverse(model_matrix));
	local_position = vpos;
	normal = normalize(vec3(normalMatrix * vec4(norm,1)));
	lightspacepos =  light_vp * model_matrix * vec4(vpos, 1.0f);
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vpos, 1.0f);
}
