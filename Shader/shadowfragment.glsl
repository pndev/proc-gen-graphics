#version 400

in vec2 position;

out vec3 color;

void main()
{
	float depth = position.x / position.y  * 0.5f + 0.5f;
	float M1 = depth;
	float dx = dFdx(depth);
	float dy = dFdy(depth);
	float M2 = depth * depth + 0.25*(dx*dx+dy*dy);

	color = vec3(M1, M2, 0.0f);
}
