#include "IslandGenerator.h"

#include "Algorithms.h"

#include <glm\glm.hpp>
#include <glm\gtc\constants.hpp>

#include <algorithm>

static float PI = glm::pi<float>();

pndev::IslandGenerator::IslandGenerator(const pndev::IslandGenerationSettings& settings) :
	settings_(settings)
{
	settings_.x_resolution = settings_.x_resolution < 2 ? 1 : settings_.x_resolution;
	settings_.y_resolution = settings_.y_resolution < 2 ? 1 : settings_.y_resolution;
	settings_.noise_octaves = settings_.noise_octaves < 1 ? 1 : settings_.noise_octaves;
}

pndev::IslandGenerator::~IslandGenerator()
{

}

bool pndev::IslandGenerator::generateIsland(int seed, pndev::IslandMesh& mesh, BoundingBox* boundingBox) const
{
	if (settings_.sides < 3)
		return false;

	mesh.vertices.clear();
	mesh.indices.clear();
	mesh.normals.clear();

	int initCap = 2 * (settings_.x_resolution - 1)*(settings_.y_resolution - 1) * 12;

	mesh.vertices.reserve(initCap);
	mesh.normals.reserve(initCap);
	mesh.indices.reserve(initCap);

	std::vector<glm::vec2> sides;
	std::vector<unsigned int> trianglePartition;
	std::vector<unsigned int> sideVertexIndices;
	sides.reserve(settings_.sides);
	trianglePartition.reserve(initCap);

	generateSides(seed, sides);
	generateTop(seed, mesh, sides, trianglePartition, sideVertexIndices);
	smoothSides(mesh, sides, sideVertexIndices, trianglePartition);
	int botOffset = mesh.vertices.size();
	generateBot(seed, mesh, sideVertexIndices);
	smoothBot(mesh, botOffset, sides, sideVertexIndices, trianglePartition);
	calculateNormalsAndAABB(mesh, boundingBox);

	return true;
}

void pndev::IslandGenerator::generateSides(int seed, std::vector<glm::vec2>& vertices) const
{
	for (int i = 0; i < settings_.sides; i++)
	{
		glm::vec2 dir(cos(2 * PI*i / settings_.sides), sin(2 * PI*i / settings_.sides));
		float scale = sampleSmoothNoise(seed, dir.x, dir.y, 1, 1);
		vertices.push_back(dir*settings_.base_radius*glm::clamp(scale, 0.5f, 1.0f));
	}
	vertices.push_back(vertices[0]);
}

void pndev::IslandGenerator::generateTop(int seed, IslandMesh& mesh, const std::vector<glm::vec2>& sides, std::vector<unsigned int>& part, std::vector<unsigned int>& sideVertexIndices) const
{
	std::vector<float> offsetX, offsetY;

	smoothNoise(offsetX, settings_.x_resolution, settings_.y_resolution, 1, 1, seed);
	smoothNoise(offsetY, settings_.x_resolution, settings_.y_resolution, 1, 1, seed);

	int numvertices = 0;

	float dx = 2 * settings_.base_radius / (settings_.x_resolution - 1);
	float dy = 2 * settings_.base_radius / (settings_.y_resolution - 1);

	for (int i = 0; i < settings_.x_resolution - 1; i++)
	{
		for (int j = 0; j < settings_.y_resolution - 1; j++)
		{
			int i1, i2, i3, i4;

			i1 = i*settings_.y_resolution + j;
			i2 = i*settings_.y_resolution + j + 1;
			i3 = i*settings_.y_resolution + j + settings_.y_resolution + 1;
			i4 = i*settings_.y_resolution + j + settings_.y_resolution;

			float x = 2 * settings_.base_radius * i / (settings_.x_resolution - 1) - settings_.base_radius;
			float y = 2 * settings_.base_radius * j / (settings_.y_resolution - 1) - settings_.base_radius;

			float aox = offsetX[i1] * dx / 2 - dx / 4;
			float aoy = offsetY[i1] * dy / 2 - dy / 4;
			float box = offsetX[i2] * dx / 2 - dx / 4;
			float boy = offsetY[i2] * dy / 2 - dy / 4;
			float cox = offsetX[i3] * dx / 2 - dx / 4;
			float coy = offsetY[i3] * dy / 2 - dy / 4;
			float dox = offsetX[i4] * dx / 2 - dx / 4;
			float doy = offsetY[i4] * dy / 2 - dy / 4;

			glm::vec3 a(x + aox, 0, y + aoy);
			glm::vec3 b(x + box, 0, y + dy + boy);
			glm::vec3 c(x + dx + cox, 0, y + dy + coy);
			glm::vec3 d(x + dx + dox, 0, y + doy);

			float scale = 1;
			{
				float scaleh = 1;
				for (int i = 0; i < settings_.noise_octaves; i++)
				{
					float s1 = sampleSmoothNoise(seed, a.x, a.z, settings_.noise_scale*scaleh, settings_.noise_scale*scaleh);
					float s2 = sampleSmoothNoise(seed, b.x, b.z, settings_.noise_scale*scaleh, settings_.noise_scale*scaleh);
					float s3 = sampleSmoothNoise(seed, c.x, c.z, settings_.noise_scale*scaleh, settings_.noise_scale*scaleh);
					float s4 = sampleSmoothNoise(seed, d.x, d.z, settings_.noise_scale*scaleh, settings_.noise_scale*scaleh);

					a.y += s1*s1* scale;
					b.y += s2*s2* scale;
					c.y += s3*s3* scale;
					d.y += s4*s4* scale;
					scale *= settings_.noise_fallback;
					scaleh *= settings_.scale_fallback;
				}
			}

			if (settings_.noise_fallback != 1)
			{
				scale = (scale - 1) / (settings_.noise_fallback - 1);
			}

			a.y *= settings_.max_y*scale;
			b.y *= settings_.max_y*scale;
			c.y *= settings_.max_y*scale;
			d.y *= settings_.max_y*scale;

			int ain = -1;
			int bin = -1;
			int cin = -1;
			int din = -1;

			int aCone = -1;
			int bCone = -1;
			int cCone = -1;
			int dCone = -1;

			for (int si = 0; si < sides.size() - 1; si++)
			{
				if (inTriangle(a, glm::vec3(sides[si].x, 0, sides[si].y), glm::vec3(0,0,0), glm::vec3(sides[si + 1].x, 0, sides[si + 1].y)))
				{
					aCone = ain = si;
				}
				else if (inCone(a, glm::vec3(0, 0, 0), glm::vec3(sides[si].x, 0, sides[si].y), glm::vec3(sides[si + 1].x, 0, sides[si + 1].y)))
				{
					aCone = si;
				}
				if (inTriangle(b, glm::vec3(sides[si].x, 0, sides[si].y), glm::vec3(0, 0, 0), glm::vec3(sides[si + 1].x, 0, sides[si + 1].y)))
				{
					bCone = bin = si;
				}
				else if (inCone(b, glm::vec3(0, 0, 0), glm::vec3(sides[si].x, 0, sides[si].y), glm::vec3(sides[si + 1].x, 0, sides[si + 1].y)))
				{
					bCone = si;
				}
				if (inTriangle(c, glm::vec3(sides[si].x, 0, sides[si].y), glm::vec3(0, 0, 0), glm::vec3(sides[si + 1].x, 0, sides[si + 1].y)))
				{
					cCone = cin = si;
				}
				else if (inCone(c, glm::vec3(0, 0, 0), glm::vec3(sides[si].x, 0, sides[si].y), glm::vec3(sides[si + 1].x, 0, sides[si + 1].y)))
				{
					cCone = si;
				}
				if (inTriangle(d, glm::vec3(sides[si].x, 0, sides[si].y), glm::vec3(0, 0, 0), glm::vec3(sides[si + 1].x, 0, sides[si + 1].y)))
				{
					dCone = din = si;
				}
				else if (inCone(d, glm::vec3(0, 0, 0), glm::vec3(sides[si].x, 0, sides[si].y), glm::vec3(sides[si + 1].x, 0, sides[si + 1].y)))
				{
					dCone = si;
				}
			}


			if (ain != -1 || bin != -1 || cin != -1)
			{
				mesh.vertices.push_back(a);
				mesh.vertices.push_back(b);
				mesh.vertices.push_back(c);

				part.push_back(aCone);
				part.push_back(bCone);
				part.push_back(cCone);

				mesh.indices.push_back(numvertices);
				mesh.indices.push_back(numvertices + 1);
				mesh.indices.push_back(numvertices + 2);

				if (ain == -1)
				{
					sideVertexIndices.push_back(numvertices);
				}
				if (bin == -1)
				{
					sideVertexIndices.push_back(numvertices + 1);
				}
				if (cin == -1)
				{
					sideVertexIndices.push_back(numvertices + 2);
				}

				numvertices += 3;
			}

			if (ain != -1 || cin != -1 || din != -1)
			{
				mesh.vertices.push_back(a);
				mesh.vertices.push_back(c);
				mesh.vertices.push_back(d);

				part.push_back(aCone);
				part.push_back(cCone);
				part.push_back(dCone);

				mesh.indices.push_back(numvertices);
				mesh.indices.push_back(numvertices + 1);
				mesh.indices.push_back(numvertices + 2);

				if (ain == -1)
				{
					sideVertexIndices.push_back(numvertices);
				}
				if (cin == -1)
				{
					sideVertexIndices.push_back(numvertices + 1);
				}
				if (din == -1)
				{
					sideVertexIndices.push_back(numvertices + 2);
				}

				numvertices += 3;
			}
		}
	}
}

void pndev::IslandGenerator::calculateNormalsAndAABB(IslandMesh& mesh, BoundingBox* boundingBox) const
{
	float lx = 0, ly = 0, lz = 0; // lowest coordinate components for AABB
	float mx = 0, my = 0, mz = 0; // highest coordinate components for AABB

	for (int i = 0; i < mesh.indices.size() / 3; i++)
	{
		glm::vec3 a = mesh.vertices[mesh.indices[3*i]];
		glm::vec3 b = mesh.vertices[mesh.indices[3*i + 1]];
		glm::vec3 c = mesh.vertices[mesh.indices[3*i + 2]];

		if (a.x < lx) lx = a.x;
		if (b.x < lx) lx = b.x;
		if (c.x < lx) lx = c.x;

		if (a.y < ly) ly = a.y;
		if (b.y < ly) ly = b.y;
		if (c.y < ly) ly = c.y;

		if (a.z < lz) lz = a.z;
		if (b.z < lz) lz = b.z;
		if (c.z < lz) lz = c.z;

		if (a.x > mx) mx = a.x;
		if (b.x > mx) mx = b.x;
		if (c.x > mx) mx = c.x;
					  
		if (a.y > my) my = a.y;
		if (b.y > my) my = b.y;
		if (c.y > my) my = c.y;
					  
		if (a.z > mz) mz = a.z;
		if (b.z > mz) mz = b.z;
		if (c.z > mz) mz = c.z;

		glm::vec3 n21(b - a), n22(c - a);
		glm::vec3 n2 = glm::cross(n21, n22);

		mesh.normals.push_back(n2);
		mesh.normals.push_back(n2);
		mesh.normals.push_back(n2);
	}
	if (boundingBox != nullptr)
	{
		boundingBox->setBase(glm::vec3(lx, ly, lz));
		boundingBox->setSides(glm::vec3(mx - lx, my - ly, mz - lz));
	}
}

void pndev::IslandGenerator::smoothSides(IslandMesh& mesh, const std::vector<glm::vec2>& sides, const std::vector<unsigned int>& sideVertices, const std::vector<unsigned int>& part) const
{

	// smooth out the steps on the boundary
	for (int i = 0; i < sideVertices.size(); i++)
	{
		glm::vec3 v = mesh.vertices[sideVertices[i]];
		glm::vec2 v1 = sides[part[sideVertices[i]]];
		glm::vec2 v2 = sides[(part[sideVertices[i]] + 1) % sides.size()];
		
		float a11 = v.x;
		float a21 = v.z;
		float a12 = v2.x - v1.x;
		float a22 = v2.y - v1.y;
		float c1 = v2.x;
		float c2 = v2.y;
		float x = (c2 - c1*a21 / a11) / (a22 - a21 / a11*a12);
		
		glm::vec2 interpolated = v1*x + v2*(1 - x);
		mesh.vertices[sideVertices[i]].x = interpolated.x;
		mesh.vertices[sideVertices[i]].z = interpolated.y;
	}

	// smooth out the elevation
	for (int i = 0; i < mesh.vertices.size(); i++)
	{
		int in = part[i];
		glm::vec3 v = mesh.vertices[i];
		glm::vec2 v1 = sides[in];
		glm::vec2 v2 = sides[(in + 1) % sides.size()];

		glm::vec2 l = v2 - v1;
		glm::vec3 l3(l.x, 0, l.y);
		float a11 = v.x;
		float a21 = v.z;
		float a12 = v2.x - v1.x;
		float a22 = v2.y - v1.y;
		float c1 = v2.x;
		float c2 = v2.y;
		float x = (c2 - c1*a21 / a11) / (a22 - a21 / a11*a12);

		glm::vec2 interpolated = v1*x + v2*(1 - x);

		mesh.vertices[i].y *= glm::length(glm::vec2(v.x, v.z) - interpolated) / glm::length(interpolated);
	}
	
	for (int i = 0; i < sideVertices.size(); i++)
	{
		
	}
}

// smooth out the bot elevation towards the boundary
void pndev::IslandGenerator::smoothBot(IslandMesh & mesh, int indexOffset, const std::vector<glm::vec2>& sides, const std::vector<unsigned int>& sideVertices, const std::vector<unsigned int>& part) const
{
	for (int i = indexOffset; i < mesh.vertices.size(); i++)
	{
		int bsi = (i % 3 == 1 ? i : i % 3 == 0 ? i + 2 : i - 2) - indexOffset;
		int in = part[bsi];
		glm::vec3 v = mesh.vertices[i];
		glm::vec2 v1 = sides[in];
		glm::vec2 v2 = sides[(in + 1) % sides.size()];

		glm::vec2 l = v2 - v1;
		glm::vec3 l3(l.x, 0, l.y);
		float a11 = v.x;
		float a21 = v.z;
		float a12 = v2.x - v1.x;
		float a22 = v2.y - v1.y;
		float c1 = v2.x;
		float c2 = v2.y;
		float x = (c2 - c1*a21 / a11) / (a22 - a21 / a11*a12);

		glm::vec2 interpolated = v1*x + v2*(1 - x);
		float s = glm::length(glm::vec2(v.x, v.z) - interpolated);
		mesh.vertices[i].y *= sqrt(s / glm::length(interpolated));
		mesh.vertices[i].y = glm::min(s/settings_.base_radius*settings_.min_y, mesh.vertices[i].y);
	}
}

void pndev::IslandGenerator::generateBot(int seed, IslandMesh& mesh, const std::vector<unsigned int>& sideVertexIndices) const
{
	int n = mesh.vertices.size();

	for (int i = 0; i < n / 3; i++)
	{
		glm::vec3 a = mesh.vertices[3 * i + 2];
		glm::vec3 b = mesh.vertices[3 * i + 1];
		glm::vec3 c = mesh.vertices[3 * i];

		float topa = a.y;
		float topb = b.y;
		float topc = c.y;

		a.y = 0;
		b.y = 0;
		c.y = 0;

		float scale = 1;
		float scaleh = 1;
		for (int i = 0; i < settings_.noise_octaves; i++)
		{
			a.y += sampleSmoothNoise(seed + 1, a.x, a.z, settings_.noise_scale*scaleh, settings_.noise_scale*scaleh) * scale;
			b.y += sampleSmoothNoise(seed + 1, b.x, b.z, settings_.noise_scale*scaleh, settings_.noise_scale*scaleh) * scale;
			c.y += sampleSmoothNoise(seed + 1, c.x, c.z, settings_.noise_scale*scaleh, settings_.noise_scale*scaleh) * scale;
			scale *= settings_.noise_fallback;
		}

		if (settings_.noise_fallback != 1)
		{
			scale = (scale - 1) / (settings_.noise_fallback - 1);
		}

		a.y *= settings_.min_y*scale;
		b.y *= settings_.min_y*scale;
		c.y *= settings_.min_y*scale;

		mesh.vertices.push_back(a);
		mesh.vertices.push_back(b);
		mesh.vertices.push_back(c);

		mesh.indices.push_back(n + 3 * i);
		mesh.indices.push_back(n + 3 * i + 1);
		mesh.indices.push_back(n + 3 * i + 2);
	}
	int svi = sideVertexIndices.size();

	for (int i = 0; i < svi; i++)
	{
		int tsi = sideVertexIndices[i];
		int bsi = (tsi % 3 == 1 ? tsi : tsi % 3 == 0 ? tsi + 2 : tsi - 2) + n;

		mesh.vertices[bsi] = mesh.vertices[tsi];
	}
}
