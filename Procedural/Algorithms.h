#pragma once

#include <vector>

#include <glm\glm.hpp>

// generates pseudorandom 2D white noise in the interval (0,1)
float sampleNoise(int x, int y, int seed);

// generates pseudorandom 2D smooth noise and stores it in the vector
void smoothNoise(std::vector<float>& noise, int width, int height, float xscale, float yscale, int seed);

// samples a pseudorandom 2D smooth noise
float sampleSmoothNoise(int seed, float x, float y, float xs, float ys);

/* 
	Calculates convex hull given the vertices, and stores the polygon indices in the second parameter.
	For vec3, the hull is calculated using the x and z components.
*/
void calculateConvexHull(const std::vector<glm::vec2>& vertices, std::vector<unsigned int>& hullIndices);
void calculateConvexHull(const std::vector<glm::vec3>& vertices, std::vector<unsigned int>& hullIndices);

/*
	
*/
void applyWeights(std::vector<float>& noise, const std::vector<float>& weights);

glm::vec2 cmInterpolation(const glm::vec2& P0, const glm::vec2& P1, const glm::vec2& P2, const glm::vec2& P3, float t);

bool inTriangle(const glm::vec3& pt, const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3);

bool inCone(const glm::vec3& pt, const glm::vec3& orig, const glm::vec3& v2, const glm::vec3& v3);

