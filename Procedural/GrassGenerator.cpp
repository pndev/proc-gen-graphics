#include "GrassGenerator.h"

#include <glm\gtc\constants.hpp>

#include "Algorithms.h"

pndev::GrassGenerator::GrassGenerator(GrassSettings settings) :
	settings_(settings)
{
}

pndev::GrassGenerator::~GrassGenerator()
{
}

void pndev::GrassGenerator::generateGrassPositions(std::vector<glm::vec3>& grassPositions, const std::vector<glm::vec3>& heightmapTriangles)
{
	int nseed = 0;
	int n = heightmapTriangles.size() / 3;
	for (int i = 0; i < n; i++)
	{
		glm::vec3 a = heightmapTriangles[3 * i];
		glm::vec3 b = heightmapTriangles[3 * i + 1];
		glm::vec3 c = heightmapTriangles[3 * i + 2];
		glm::vec3 n21(b - a), n22(c - a);
		glm::vec3 n2 = glm::normalize(glm::cross(n21, n22));

		float tresshold = cos(glm::radians<float>(settings_.max_slope_angle));
		float currentSlope = glm::dot(n2, glm::vec3(0, 1, 0));
		if (currentSlope < tresshold)
		{
			continue;
		}

		float triangleArea = glm::length(glm::cross(b - a, c - a)) / 2.0f;
		float slopeScale = 1;
		if (tresshold < 1) slopeScale = (currentSlope - tresshold) / (1 - tresshold);
		int numGrass = (int)(slopeScale * glm::max<int>(triangleArea * settings_.grass_density, 1));

		for (int j = 0; j < numGrass; j++)
		{
			float aw = sampleNoise(i, j, nseed++);
			float bw = sampleNoise(i, j, nseed++) * (1 - aw);
			float cw = 1 - aw - bw;

			glm::vec3 gpos = a*aw + b*bw + c*cw;
			grassPositions.push_back(gpos);
		}
	}
}

pndev::GrassMesh pndev::GrassGenerator::generateGrassMesh()
{
	GrassMesh mesh;

	for (int i = 0; i < 3; i++) 
	{
		int n = mesh.vertices.size();

		float s = sin(i / 3.0f * 2.0f * glm::pi<float>());
		float c = cos(i / 3.0f * 2.0f * glm::pi<float>());

		mesh.vertices.push_back(0.5f * glm::vec3(-c, 0, -s));
		mesh.vertices.push_back(0.5f * glm::vec3(c, 0, s));
		mesh.vertices.push_back(0.5f * glm::vec3(c, 2.0f, s));
		mesh.vertices.push_back(0.5f * glm::vec3(-c, 2.0f, -s));

		mesh.indices.push_back(n + 0);
		mesh.indices.push_back(n + 1);
		mesh.indices.push_back(n + 2);
		mesh.indices.push_back(n + 0);
		mesh.indices.push_back(n + 2);
		mesh.indices.push_back(n + 3);

		mesh.texture_coords.push_back(glm::vec2(0, 1));
		mesh.texture_coords.push_back(glm::vec2(1, 1));
		mesh.texture_coords.push_back(glm::vec2(1, 0));
		mesh.texture_coords.push_back(glm::vec2(0, 0));
	}
	return mesh;
}