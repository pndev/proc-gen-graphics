#include "Algorithms.h"

#include <math.h>
#include <random>

static int permutation[256] = {
	151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,
	142,8,99,37,240,21,10,23,190,6,148,247,120,234,75,0,26,197,62,94,252,219,
	203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168,68,175,
	74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,
	220,105,92,41,55,46,245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,
	132,187,208,89,18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,
	186,3,64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,
	59,227,47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,
	70,221,153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,
	178,185,112,104,218,246,97,228,251,34,242,193,238,210,144,12,191,179,162,
	241,81,51,145,235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,
	176,115,121,50,45,127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,
	128,195,78,66,215,61,156,180
};

void smoothNoise(std::vector<float>& noise, int width, int height, float xscale, float yscale, int seed)
{
	if (width < 1 || height < 1)
		return;

	xscale = xscale < 1 ? 1 : xscale;
	yscale = yscale < 1 ? 1 : yscale;

	noise.clear();
	noise.reserve(width*height);

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			float sx = x / xscale;
			float sy = y / yscale;

			float tx = sx - std::floor(sx);
			float ty = sy - std::floor(sy);

			int X[4];
			int Y[4];

			X[1] = (int) std::floor(sx);
			X[0] = X[1] - 1;
			X[2] = X[1] + 1;
			X[3] = X[1] + 2;

			Y[1] = (int) std::floor(sy);
			Y[0] = Y[1] - 1;
			Y[2] = Y[1] + 1;
			Y[3] = Y[1] + 2;

			float ns[4][4];

			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					ns[i][j] = sampleNoise(X[i], Y[j], seed);
				}
			}

			glm::vec2 P0, P1, P2, P3;
			P0 = cmInterpolation(glm::vec2(X[0], ns[0][0]), glm::vec2(X[1], ns[1][0]), glm::vec2(X[2], ns[2][0]), glm::vec2(X[3], ns[3][0]), tx);
			P1 = cmInterpolation(glm::vec2(X[0], ns[0][1]), glm::vec2(X[1], ns[1][1]), glm::vec2(X[2], ns[2][1]), glm::vec2(X[3], ns[3][1]), tx);
			P2 = cmInterpolation(glm::vec2(X[0], ns[0][2]), glm::vec2(X[1], ns[1][2]), glm::vec2(X[2], ns[2][2]), glm::vec2(X[3], ns[3][2]), tx);
			P3 = cmInterpolation(glm::vec2(X[0], ns[0][3]), glm::vec2(X[1], ns[1][3]), glm::vec2(X[2], ns[2][3]), glm::vec2(X[3], ns[3][3]), tx);

			glm::vec2 P = cmInterpolation(P0, P1, P2, P3, ty);

			noise.push_back(P.y);
		}
	}
}

float sampleSmoothNoise(int seed, float x, float y, float xScale, float yScale)
{
	xScale = xScale < 1 ? 1 : xScale;
	yScale = yScale < 1 ? 1 : yScale;

	x /= xScale;
	y /= yScale;

	float tx = x - std::floor(x);
	float ty = y - std::floor(y);

	int X[4];
	int Y[4];

	X[1] = (int)std::floor(x);
	X[0] = X[1] - 1;
	X[2] = X[1] + 1;
	X[3] = X[1] + 2;

	Y[1] = (int)std::floor(y);
	Y[0] = Y[1] - 1;
	Y[2] = Y[1] + 1;
	Y[3] = Y[1] + 2;

	float ns[4][4];

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			ns[i][j] = sampleNoise(X[i], Y[j], seed);
		}
	}

	glm::vec2 P0, P1, P2, P3;
	P0 = cmInterpolation(glm::vec2(X[0], ns[0][0]), glm::vec2(X[1], ns[1][0]), glm::vec2(X[2], ns[2][0]), glm::vec2(X[3], ns[3][0]), tx);
	P1 = cmInterpolation(glm::vec2(X[0], ns[0][1]), glm::vec2(X[1], ns[1][1]), glm::vec2(X[2], ns[2][1]), glm::vec2(X[3], ns[3][1]), tx);
	P2 = cmInterpolation(glm::vec2(X[0], ns[0][2]), glm::vec2(X[1], ns[1][2]), glm::vec2(X[2], ns[2][2]), glm::vec2(X[3], ns[3][2]), tx);
	P3 = cmInterpolation(glm::vec2(X[0], ns[0][3]), glm::vec2(X[1], ns[1][3]), glm::vec2(X[2], ns[2][3]), glm::vec2(X[3], ns[3][3]), tx);

	return glm::clamp(cmInterpolation(P0, P1, P2, P3, ty).y, 0.0f, 1.0f);

}

static bool lessThan(const glm::vec2& lhs, const glm::vec2& rhs)
{
	if (lhs.x < rhs.x) return true;
	if (lhs.x == rhs.x) return lhs.y < rhs.y;
	return false;
}

static bool lessThan(const glm::vec3& lhs, const glm::vec3& rhs)
{
	if (lhs.x < rhs.x) return true;
	if (lhs.x == rhs.x) return lhs.z < rhs.z;
	return false;
}

template<class T>
static void sortIndicesByVectors(std::vector<unsigned int>& indices, const std::vector<T>& vertices)
{
	bool swapped = false;
	int k = 0;
	indices.clear();
	indices.reserve(vertices.size());
	for (int i = 0; i < vertices.size(); i++)
	{
		indices.push_back(i);
	}
	do
	{
		swapped = false;
		for (int i = 1; i < vertices.size(); i++)
		{
			if (lessThan(vertices[indices[i]], vertices[indices[i - 1]]))
			{
				int a = indices[i - 1];
				indices[i - 1] = indices[i];
				indices[i] = a;

				swapped = true;
			}
		}
	} while (swapped);
}

static float cross(const glm::vec2 &O, const glm::vec2 &A, const glm::vec2 &B)
{
	return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
}

static float cross(const glm::vec3 &O, const glm::vec3 &A, const glm::vec3 &B)
{
	return (A.x - O.x) * (B.z - O.z) - (A.z - O.z) * (B.x - O.x);
}

template<class T>
void calculateConvexHullTemplate(const std::vector<T>& vertices, std::vector<unsigned int>& hullIndices)
{
	std::vector<unsigned int> sorted;
	sortIndicesByVectors(sorted, vertices);

	unsigned int n = sorted.size(), k = 0;
	if (n == 1) return;

	hullIndices.clear();
	hullIndices.resize(2 * n);

	for (int i = 0; i < n; ++i)
	{
		while (k >= 2 && cross(vertices[hullIndices[k - 2]], vertices[hullIndices[k - 1]], vertices[sorted[i]]) <= 0)
		{
			k--;
		}
		hullIndices[k] = (sorted[i]);
		k++;
	}

	for (int i = n - 2, t = k + 1; i >= 0; i--)
	{
		while (k >= t && cross(vertices[hullIndices[k - 2]], vertices[hullIndices[k - 1]], vertices[sorted[i]]) <= 0)
		{
			k--;
		}
		hullIndices[k] = (sorted[i]);
		k++;
	}
	if (k < 2) return;
	hullIndices.resize(k - 1);
}

void calculateConvexHull(const std::vector<glm::vec2>& vertices, std::vector<unsigned int>& hullIndices)
{
	calculateConvexHullTemplate<glm::vec2>(vertices, hullIndices);
}

void calculateConvexHull(const std::vector<glm::vec3>& vertices, std::vector<unsigned int>& hullIndices)
{
	calculateConvexHullTemplate<glm::vec3>(vertices, hullIndices);
}

void applyWeights(std::vector<float>& noise, const std::vector<float>& weights)
{
	if (noise.size() != weights.size())
		return;

	for (int i = 0; i < noise.size(); i++)
	{
		noise[i] *= weights[i];
	}
}

glm::vec2 cmInterpolation(const glm::vec2& P0, const glm::vec2& P1, const glm::vec2& P2, const glm::vec2& P3, float t)
{
	if (t < 0) return P1;
	if (t > 1) return P2;
	return 0.5f *((2.0f * P1) + 
		(-P0 + P2) * t + 
		(2.0f * P0 - 5.0f * P1 + 4.0f * P2 - P3) * t*t + 
		(-P0 + 3.0f * P1 - 3.0f * P2 + P3) * t*t*t);
}

float sampleNoise(int x, int y, int seed)
{
	int h = (((x << 13) ^ x) * 7919 + ((y << 7) ^ y) * 2633) ^ seed;
	float ret = permutation[h & 0xff] / float(0xff);
	return ret;
}

static float sign(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3)
{
	return (p1.x - p3.x) * (p2.z - p3.z) - (p2.x - p3.x) * (p1.z - p3.z);
}

bool inTriangle(const glm::vec3& pt, const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3)
{
	bool b1, b2, b3;

	b1 = sign(pt, v1, v2) <= 0.0f;
	b2 = sign(pt, v2, v3) <= 0.0f;
	b3 = sign(pt, v3, v1) <= 0.0f;

	return ((b1 == b2) && (b2 == b3));
}

bool inCone(const glm::vec3 & pt, const glm::vec3 & v1, const glm::vec3 & v2, const glm::vec3 & v3)
{
	bool b1, b3;

	b1 = sign(pt, v1, v2) >= 0.0f;
	b3 = sign(pt, v3, v1) >= 0.0f;

	return b1 && b3;
}
