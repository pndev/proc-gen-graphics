#pragma once

#include "../Core/Object.h"
#include "../Core/ShaderProgram.h"
#include "../Core/TextureLoader.h"

#include <future>
#include <atomic>

#include "IslandGenerator.h"
#include "GrassGenerator.h"

namespace pndev 
{
	struct IslandSettings
	{
		float float_velocity;
		float float_elevation;
		float max_tilt_angle;

		glm::vec3 base_position;

		Texture grass;
		Texture rock;
		Texture grass_blades;

		float rock_inverse_scale = 1;
		float grass_inverse_scale = 1;

		float wind_speed;
		glm::vec3 wind_direction;

		float grass_distance;
		unsigned int max_grass_instances;
	};

	class FloatingIsland : public Object
	{
	public:
		FloatingIsland(
			int seed,
			const IslandSettings& settings,
			const IslandGenerator& generator,
			const GrassGenerator& grassGenerator,
			const ShaderProgram& shader,
			const ShaderProgram& grassShader);
		~FloatingIsland();

		/* returns settings reference */
		IslandSettings& settings();

		void initialize(const Scene& scene);
		void update(const Scene& scene);

	protected:
		void renderInternal();
		void renderShadowmapInternal();

	private:

		std::atomic<bool> loaded_;
		bool buffers_ready_;
		std::future<void> asnyc_generator_;

		void generateIslandBuffers();
		void generateGrassBuffers();
		void generateTreeBuffers();

		GLuint vao_;
		GLuint vertex_buffer_;
		GLuint normal_buffer_;
		GLuint index_buffer_;

		GLuint grass_vao_;
		GLuint grass_vertex_buffer_;
		GLuint grass_texcoord_buffer_;
		GLuint grass_index_buffer_;
		GLuint grass_instance_position_buffer_;

		IslandMesh mesh_; 
		GrassMesh grass_mesh_;
		std::vector<glm::vec3> grass_positions_;

		IslandGenerator generator_;
		GrassGenerator grass_generator_;

		IslandSettings settings_;
		int seed_;
	};

}