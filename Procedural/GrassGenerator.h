#pragma once

#include <glm\glm.hpp>

#include <vector>

namespace pndev
{
	struct GrassMesh
	{
		std::vector<glm::vec3> vertices;
		std::vector<unsigned int> indices;
		std::vector<glm::vec2> texture_coords;
	};

	/*
		Settings for grass generation.
	*/
	struct GrassSettings
	{
		/*
			Number of grass object per unit area
		*/
		float grass_density; 

		/*
			Maximum angle where the grass can grow
		*/
		float max_slope_angle;
	};

	class GrassGenerator
	{
	public:
		GrassGenerator(GrassSettings defaultSettings);
		~GrassGenerator();
		
		/*
			Generates the positions where the grass can grow.
			This method assumes nearly uniform triangle mesh. (The sizes of the triangles are approximately the same.)
			The number of grass objects per triangle is ~ floor(grass_density*triangle_area).
		*/
		void generateGrassPositions(std::vector<glm::vec3>& grassPositions, const std::vector<glm::vec3>& heightmapTriangles);

		/*
			Generates the mesh for one grass object.
		*/
		GrassMesh generateGrassMesh();

	private:
		GrassSettings settings_;
	};

}