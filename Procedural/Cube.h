#pragma once

#include "..\Core\Object.h"


namespace pndev {

	class Scene;

	class Cube : public Object
	{
	public:
		Cube(const ShaderProgram& shader);
		~Cube();

		void update(const Scene& scene);
		void initialize();

	private:

		GLuint vao;

	protected:

		void renderInternal();
		void renderShadowmapInternal();

	};

}