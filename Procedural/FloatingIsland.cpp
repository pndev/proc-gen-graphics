#include "FloatingIsland.h"

#include <future>

#include "..\Core\Time.h"
#include "..\Core\Scene.h"

#include <glm\gtc\constants.hpp>
#include <glm\gtc\type_ptr.hpp>

static float PI = glm::pi<float>();

pndev::FloatingIsland::FloatingIsland(
	int seed, 
	const IslandSettings& settings, 
	const IslandGenerator& generator,
	const GrassGenerator& grassGenerator,
	const ShaderProgram& shader,
	const ShaderProgram& grassShader) :
	Object(shader),
	generator_(generator),
	seed_(seed),
	settings_(settings),
	grass_generator_(grassGenerator),
	loaded_(false),
	buffers_ready_(false)
{
	this->bounding_box_ = new BoundingBox();

	textures_["grass_texture"] = settings_.grass.texture_id;
	textures_["rock_texture"] = settings_.rock.texture_id;
	textures_["grass_blade_texture"] = settings_.grass_blades.texture_id;

	addShader(grassShader);
}

pndev::FloatingIsland::~FloatingIsland()
{
	if (bounding_box_ != nullptr) delete bounding_box_;
}

pndev::IslandSettings& pndev::FloatingIsland::settings()
{
	return settings_;
}

void pndev::FloatingIsland::renderInternal()
{
	if (!buffers_ready_) return;

	shaders_[0].use();
	shaders_[0].setProperty("grass_scale", settings_.grass_inverse_scale);
	shaders_[0].setProperty("rock_scale", settings_.rock_inverse_scale);

	glBindVertexArray(vao_); 
	glDrawElements(GL_TRIANGLES, mesh_.indices.size(), GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);

	shaders_[1].use();
	shaders_[1].setProperty("time", (float) Time::getElapsedTime());
	shaders_[1].setProperty("wind_speed", settings_.wind_speed);
	shaders_[1].setProperty("wind_direction", settings_.wind_direction);

	glDisable(GL_CULL_FACE);
	glBindVertexArray(grass_vao_);
	glDrawElementsInstanced(GL_TRIANGLES, grass_mesh_.indices.size(), GL_UNSIGNED_INT, nullptr, grass_positions_.size());
	glBindVertexArray(0);
	glEnable(GL_CULL_FACE);
}

void pndev::FloatingIsland::renderShadowmapInternal()
{
	if (!buffers_ready_) return;

	glBindVertexArray(vao_);
	glDrawElements(GL_TRIANGLES, mesh_.indices.size(), GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
}

void pndev::FloatingIsland::generateIslandBuffers()
{
	assert(mesh_.vertices.size() >= 1);
	assert(mesh_.indices.size() >= 1);
	assert(mesh_.normals.size() >= 1);

	const float* vertices = glm::value_ptr(mesh_.vertices[0]);
	const float* normals = glm::value_ptr(mesh_.normals[0]);
	const unsigned int* indices = &mesh_.indices[0];

	glGenBuffers(1, &vertex_buffer_);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_);
	glBufferData(GL_ARRAY_BUFFER, mesh_.vertices.size() * sizeof(glm::vec3), vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &normal_buffer_);
	glBindBuffer(GL_ARRAY_BUFFER, normal_buffer_);
	glBufferData(GL_ARRAY_BUFFER, mesh_.normals.size() * sizeof(glm::vec3), normals, GL_STATIC_DRAW);

	glGenBuffers(1, &index_buffer_);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh_.indices.size() * sizeof(unsigned int), indices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &vao_);

	glBindVertexArray(vao_);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ARRAY_BUFFER, normal_buffer_);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_);

	glBindVertexArray(0);
}

void pndev::FloatingIsland::generateGrassBuffers()
{
	assert(grass_mesh_.vertices.size() >= 1);
	assert(grass_mesh_.indices.size() >= 1);
	assert(grass_mesh_.texture_coords.size() >= 1);

	const float* vertices = glm::value_ptr(grass_mesh_.vertices[0]);
	const float* textureCoords = glm::value_ptr(grass_mesh_.texture_coords[0]);
	const unsigned int* indices = &grass_mesh_.indices[0];
	const float* grassPositions = glm::value_ptr(grass_positions_[0]);

	glGenBuffers(1, &grass_vertex_buffer_);
	glBindBuffer(GL_ARRAY_BUFFER, grass_vertex_buffer_);
	glBufferData(GL_ARRAY_BUFFER, grass_mesh_.vertices.size() * sizeof(glm::vec3), vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &grass_texcoord_buffer_);
	glBindBuffer(GL_ARRAY_BUFFER, grass_texcoord_buffer_);
	glBufferData(GL_ARRAY_BUFFER, grass_mesh_.texture_coords.size() * sizeof(glm::vec2), textureCoords, GL_STATIC_DRAW);

	glGenBuffers(1, &grass_index_buffer_);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, grass_index_buffer_);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, grass_mesh_.indices.size() * sizeof(unsigned int), indices, GL_STATIC_DRAW);

	glGenBuffers(1, &grass_instance_position_buffer_);
	glBindBuffer(GL_ARRAY_BUFFER, grass_instance_position_buffer_);
	glBufferData(GL_ARRAY_BUFFER, grass_positions_.size() * sizeof(glm::vec3), grassPositions, GL_STATIC_DRAW);

	glGenVertexArrays(1, &grass_vao_);

	glBindVertexArray(grass_vao_);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, grass_vertex_buffer_);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ARRAY_BUFFER, grass_texcoord_buffer_);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ARRAY_BUFFER, grass_instance_position_buffer_);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glVertexAttribDivisor(2, 1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, grass_index_buffer_);

	glBindVertexArray(0);
}

void pndev::FloatingIsland::generateTreeBuffers()
{

}

void pndev::FloatingIsland::update(const Scene& scene)
{
	if (!loaded_) return;

	if (!buffers_ready_)
	{
		asnyc_generator_.get();
		generateIslandBuffers();
		generateGrassBuffers();
		generateTreeBuffers();
		buffers_ready_ = true;
	}

	float elapsed = Time::getElapsedTime()*settings_.float_velocity * 2 * PI;
	position_ = settings_.base_position + glm::vec3(0, settings_.float_elevation*sin(elapsed), 0);
	rotation_ = glm::angleAxis(glm::radians<float>(sin(elapsed)*settings_.max_tilt_angle),
		glm::vec3(sin(elapsed), 0, cos(elapsed)));


}

void pndev::FloatingIsland::initialize(const Scene& scene)
{
	scene.getInputManager()->registerKeyCallback(
		[this](int key, int scan, int action, int mods)
		{
			if (loaded_ && buffers_ready_ && (key == GLFW_KEY_N || key == GLFW_KEY_M) && action == GLFW_PRESS) {
				seed_ = key == GLFW_KEY_N ? seed_ + 1 : seed_ - 1;
				loaded_ = false;
				buffers_ready_ = false;
				asnyc_generator_ = std::async([this]()
				{
					mesh_.indices.clear();
					mesh_.vertices.clear();
					mesh_.normals.clear();
					grass_positions_.clear();

					generator_.generateIsland(seed_, mesh_, bounding_box_);
					grass_generator_.generateGrassPositions(grass_positions_, mesh_.vertices);
					grass_mesh_ = grass_generator_.generateGrassMesh();
					loaded_ = true;
				});
			}
		}
	);

	asnyc_generator_ = std::async([this]() 
	{ 
		generator_.generateIsland(seed_, mesh_, bounding_box_);
		grass_generator_.generateGrassPositions(grass_positions_, mesh_.vertices);
		grass_mesh_ = grass_generator_.generateGrassMesh();
		loaded_ = true;
	});
}
