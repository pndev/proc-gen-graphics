#pragma once

#include <vector>

#include <glm\glm.hpp>

#include "..\Core\BoundingBox.h"

namespace pndev {

	struct IslandMesh
	{
		std::vector<glm::vec3> vertices;
		std::vector<unsigned int> indices;
		std::vector<glm::vec3> normals;
	};

	struct IslandGenerationSettings
	{
		int sides; // the sides of the base polygon

		int x_resolution; // vertex count per base_radius in x direction
		int y_resolution; // vertex count per base_radius in y direction

		float max_y; // maximum height
		float min_y; // minimum height

		float base_radius; // maximum radius of the island

		float noise_scale; // (x,z) scale of the noise function
		int noise_octaves; // number of noise samples per vertex
		float scale_fallback; // (x,z) scale multiplier of noise function per iteration
		float noise_fallback; // y scale multiplier of noise function per iteration
	};

	class IslandGenerator
	{
	public:

		IslandGenerator(const IslandGenerationSettings& settings);
		~IslandGenerator();

		/*
			First, generates the sides of the island.
			Triangulate the area, and calculate the elevation with smooth noise
			Offsets are also added to (x,z) coordinates for a more randomized look.
			Smooth out the elevation, also remove the zigzags at the boundary.
			Calculate the bot, then smooth out.
			Finally, normal and AABB is calculated.
		*/
		bool generateIsland(int seed, IslandMesh& mesh, BoundingBox* boundingBox) const;

	private:

		/*
			Generates the side vertices by randomly offsetting a regular polygon's vertices.
		*/
		void generateSides(int seed, std::vector<glm::vec2>& vertices) const;
		
		/*
			Generates the top/bot elevation using smooth noise.
			The 'part' argument contains the triangle location index used for smoothing out the elevation towards the boundary.
			'sideVertexIndices' contains the boundary vertex indices.
		*/
		void generateTop(int seed, IslandMesh& mesh, const std::vector<glm::vec2>& sides, std::vector<unsigned int>& part, std::vector<unsigned int>& sideVertexIndices) const;
		void generateBot(int seed, IslandMesh& mesh, const std::vector<unsigned int>& sideVertexIndices) const;

		void calculateNormalsAndAABB(IslandMesh& mesh, BoundingBox* boundingBox) const;

		void smoothSides(IslandMesh & mesh, const std::vector<glm::vec2>& sides, const std::vector<unsigned int>& sideVertices, const std::vector<unsigned int>& part) const;
		
		void smoothBot(IslandMesh & mesh, int indexOffset, const std::vector<glm::vec2>& sides, const std::vector<unsigned int>& sideVertices, const std::vector<unsigned int>& part) const;

		IslandGenerationSettings settings_;
	};

}