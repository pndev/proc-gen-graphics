// Metalomorphism.cpp : Defines the entry point for the console application.
//
#ifdef TEST_CONFIG

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <string>
#include <chrono>


#include "Core/Scene.h"
#include "Procedural/Cube.h"
#include "Procedural/FloatingIsland.h"
#include "Core/Camera.h"
#include "Core/Time.h"
#include "Core/Scripts/DynamicLight.h"

#endif

#include "Core/Engine.h"
#include "Core/Config/NameValueFileLoader.h"

int main(int argc, const char** argv)
{
	pndev::ContextSettings settings;

	pndev::NameValueFileLoader<std::string, std::string> loader;

	auto nv = loader.getNameValuePairs("Config/settings.conf");
	try 
	{
		settings.opengl_major_version = std::stoi(nv.at("opengl_major_version"));
		settings.opengl_minor_version = std::stoi(nv.at("opengl_minor_version"));
		settings.resource_path = nv.at("resource_path");
		settings.window_width = std::stoi(nv.at("window_width"));
		settings.window_height = std::stoi(nv.at("window_height"));
		settings.window_title = nv.at("window_title");

		pndev::Engine engine(settings);
		engine.run();
	}
	catch (const std::exception& e)
	{
		std::cout << "Fatal error during program launch! (" << e.what() << ")" << std::endl;
	}
	while (1) {};
}

#ifdef TEST_CONFIG
static const float PI = 3.141592f;

struct ContextSettings {

	std::string windowTitle;
	int windowWidth;
	int windowHeight;
	int oglMajorVersion;
	int oglMinorVersion;
	bool wireframe;

	ContextSettings()
	{
		this->windowTitle = "OpenGL Application";
		this->windowWidth = 1024;
		this->windowHeight = 768;
		this->oglMajorVersion = 4;
		this->oglMinorVersion = 0;
		this->wireframe = false;
	}
};

struct CameraMovementFlags
{
	static const int IDLE = 0;
	static const int FORWARD = 1;
	static const int BACKWARD = 2;
	static const int RIGHT = 4;
	static const int LEFT = 8;
};


struct CameraState
{
	int movementState = CameraMovementFlags::IDLE;
	float cameraSpeed = 5.0f;
};

struct MouseData
{
	double lastX;
	double lastY;
	double deltaX;
	double deltaY;
	double currX;
	double currY;
	bool isLeftPressed;
	bool isRightPressed;
};

static ContextSettings settings;
static CameraState cameraState;
static MouseData mouseData;

static void cameraMovement(pndev::Camera* cam, int state, float deltaSecs, float cameraSpeed)
{
	if (state == CameraMovementFlags::IDLE)
		return;

	if (cam != nullptr)
	{
		if (state & CameraMovementFlags::FORWARD)
		{
			cam->setPosition(cam->getPosition() + (cam->getForward()*deltaSecs*cameraSpeed));
		}

		if (state & CameraMovementFlags::BACKWARD)
		{
			cam->setPosition(cam->getPosition() - (cam->getForward()*deltaSecs*cameraSpeed));
		}

		if (state & CameraMovementFlags::RIGHT)
		{
			cam->setPosition(cam->getPosition() + (cam->getRight()*deltaSecs*cameraSpeed));
		}

		if (state & CameraMovementFlags::LEFT)
		{
			cam->setPosition(cam->getPosition() - (cam->getRight()*deltaSecs*cameraSpeed));
		}
	}
}

static void cameraRotation(pndev::Camera* cam)
{
	if (cam != nullptr && mouseData.isRightPressed)
	{
		float dh, dv;
		int w = 1, h = 1;
		cam->getViewPort(w, h);
		dh = (float)mouseData.deltaX / w * PI;
		dv = (float)mouseData.deltaY / h * PI;
		cam->rotateByDelta(dh, dv);
	}
}

static void errorCallback(int error, const char* description)
{
	std::cerr << "Error code " << error << ": " << description << "\n";
};

static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);

	if (key == GLFW_KEY_W || key == GLFW_KEY_UP)
	{
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
		{
			cameraState.movementState = cameraState.movementState | CameraMovementFlags::FORWARD;
		}
		else if (action == GLFW_RELEASE)
		{
			cameraState.movementState = cameraState.movementState & ~CameraMovementFlags::FORWARD;
		}
	}

	if (key == GLFW_KEY_S || key == GLFW_KEY_DOWN)
	{
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
		{
			cameraState.movementState = cameraState.movementState | CameraMovementFlags::BACKWARD;
		}
		else if (action == GLFW_RELEASE)
		{
			cameraState.movementState = cameraState.movementState & ~CameraMovementFlags::BACKWARD;
		}
	}

	if (key == GLFW_KEY_A || key == GLFW_KEY_LEFT)
	{
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
		{
			cameraState.movementState = cameraState.movementState | CameraMovementFlags::LEFT;
		}
		else if (action == GLFW_RELEASE)
		{
			cameraState.movementState = cameraState.movementState & ~CameraMovementFlags::LEFT;
		}
	}

	if (key == GLFW_KEY_D || key == GLFW_KEY_RIGHT)
	{
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
		{
			cameraState.movementState = cameraState.movementState | CameraMovementFlags::RIGHT;
		}
		else if (action == GLFW_RELEASE)
		{
			cameraState.movementState = cameraState.movementState & ~CameraMovementFlags::RIGHT;
		}
	}

	if (key == GLFW_KEY_P && action == GLFW_PRESS)
	{
		settings.wireframe = !settings.wireframe;
	}
}

static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		mouseData.isRightPressed = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		mouseData.isRightPressed = false;
	}
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		mouseData.isLeftPressed = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		mouseData.isLeftPressed = false;
	}
}

static void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos)
{
	mouseData.currX = xpos;
	mouseData.currY = ypos;
}

static void cursorPositionDataUpdate()
{
	mouseData.deltaX = mouseData.currX - mouseData.lastX;
	mouseData.deltaY = mouseData.currY - mouseData.lastY;
	mouseData.lastX = mouseData.currX;
	mouseData.lastY = mouseData.currY;
}

static void framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
	settings.windowWidth = width;
	settings.windowHeight = height;
}

int testMain()
{
	glfwSetErrorCallback(errorCallback);

	if (GLFW_TRUE != glfwInit())
	{
		return -1;
	}

	std::cout << "GLFW initialized\n";

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, settings.oglMajorVersion);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, settings.oglMinorVersion);
	GLFWwindow* window = glfwCreateWindow(settings.windowWidth, settings.windowHeight, settings.windowTitle.c_str(), NULL, NULL);
	if (!window)
	{
		return -1;
	}

	glfwMakeContextCurrent(window);


	const GLenum err = glewInit();

	if (GLEW_OK != err)
	{
		std::cout << "GLEW Error: " << glewGetErrorString(err) << "\n";
		return -1;
	}

	std::cout << "GLEW initialized\n";

	std::cout << "OpenGL version: " << (char*)glGetString(GL_VERSION) << "\n";
	std::cout << "GLSL version: " << (char*)glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
	std::cout << "OpenGL vendor: " << (char*)glGetString(GL_VENDOR) << "\n";

	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
	glfwSetKeyCallback(window, keyCallback);
	glfwSetCursorPosCallback(window, cursorPositionCallback);
	glfwSetMouseButtonCallback(window, mouseButtonCallback);

	cameraState.cameraSpeed = 25;

	pndev::TextureLoader textureLoader;

	pndev::ShaderProgram terrainShader("terrain", "Shader/terrainvertex.glsl", "Shader/terrainfragment.glsl", nullptr, nullptr, nullptr);
	pndev::ShaderProgram grassShader("grass", "Shader/grassvertex.glsl", "Shader/grassfragment.glsl", nullptr, nullptr, nullptr);
	pndev::ShaderProgram skyboxShader("skybox", "Shader/skyvert.glsl", "Shader/skyfrag.glsl", nullptr, nullptr, nullptr);
	pndev::ShaderProgram simpleShadowmapShader("shadow", "Shader/shadowvertex.glsl", "Shader/shadowfragment.glsl", nullptr, nullptr, nullptr);
	pndev::ShaderProgram blurShader("blur", "Shader/fullquadvertex.glsl", "Shader/blurfragment.glsl", nullptr, nullptr, nullptr);

	pndev::Texture day = textureLoader.loadCubemap(
		"Texture/Skybox/skyboxset1/SkyboxSet1/TropicalSunnyDay/TropicalSunnyDayRight2048.png",
		"Texture/Skybox/skyboxset1/SkyboxSet1/TropicalSunnyDay/TropicalSunnyDayLeft2048.png",
		"Texture/Skybox/skyboxset1/SkyboxSet1/TropicalSunnyDay/TropicalSunnyDayUp2048.png",
		"Texture/Skybox/skyboxset1/SkyboxSet1/TropicalSunnyDay/TropicalSunnyDayDown2048.png",
		"Texture/Skybox/skyboxset1/SkyboxSet1/TropicalSunnyDay/TropicalSunnyDayBack2048.png",
		"Texture/Skybox/skyboxset1/SkyboxSet1/TropicalSunnyDay/TropicalSunnyDayFront2048.png");

	pndev::Texture night = textureLoader.loadCubemap(
		"Texture/Skybox/skyboxset1/SkyboxSet1/FullMoon/FullMoonRight2048.png",
		"Texture/Skybox/skyboxset1/SkyboxSet1/FullMoon/FullMoonLeft2048.png",
		"Texture/Skybox/skyboxset1/SkyboxSet1/FullMoon/FullMoonUp2048.png",
		"Texture/Skybox/skyboxset1/SkyboxSet1/FullMoon/FullMoonDown2048.png",
		"Texture/Skybox/skyboxset1/SkyboxSet1/FullMoon/FullMoonBack2048.png",
		"Texture/Skybox/skyboxset1/SkyboxSet1/FullMoon/FullMoonFront2048.png");

	pndev::Texture grass = textureLoader.loadTexture("Texture/Terrain/grass.jpg");
	pndev::Texture rock = textureLoader.loadTexture("Texture/Terrain/rock.jpg");
	pndev::Texture grassBlades = textureLoader.loadTexture("Texture/Terrain/grassblades.png", 4, false);

	pndev::IslandGenerationSettings islandGenerationSettings;
	islandGenerationSettings.sides = 20;
	islandGenerationSettings.scale_fallback = 0.8f;
	islandGenerationSettings.base_radius = 100;
	islandGenerationSettings.max_y = 50;
	islandGenerationSettings.min_y = -100;
	islandGenerationSettings.x_resolution = 30;
	islandGenerationSettings.y_resolution = 30;
	islandGenerationSettings.noise_fallback = 0.1f;
	islandGenerationSettings.noise_octaves = 3;
	islandGenerationSettings.noise_scale = 20;

	pndev::IslandGenerator generator(islandGenerationSettings);

	pndev::GrassSettings grassSettings;
	grassSettings.grass_density = 10.0f;
	grassSettings.max_slope_angle = 15.0f;

	pndev::GrassGenerator grassGenerator(grassSettings);

	pndev::IslandSettings islandSettings;
	islandSettings.float_elevation = 1;
	islandSettings.float_velocity = 0.1;
	islandSettings.max_tilt_angle = 1;
	islandSettings.grass = grass;
	islandSettings.rock = rock;
	islandSettings.grass_blades = grassBlades;
	islandSettings.grass_inverse_scale = 1.0f;
	islandSettings.rock_inverse_scale = 0.01f;
	islandSettings.wind_speed = 0.1f;
	islandSettings.wind_direction = glm::vec3(1.0f, 0.0f, 0.75f);

	pndev::FloatingIsland island1(1, islandSettings, generator, grassGenerator, terrainShader, grassShader);

	pndev::SkyBox skybox(skyboxShader, day, night, 20);

	pndev::DynamicLight lightScript(20.0f, glm::vec3(1, 0.9, 0.9), glm::vec3(1, 0.8, 0.7));

	pndev::DirectionalLight light;
	light.setAmbientColor(glm::vec3(0.2, 0.3, 0.3));

	pndev::SceneSettings sceneSettings;
	sceneSettings.shadowmapResolution = 1 * glm::ivec2(1024, 1024);
	pndev::Scene scene("demo", sceneSettings);

	pndev::Camera camera;
	scene.addCamera(&camera);
	scene.setActiveCamera(0);

	scene.setSkybox(&skybox);
	scene.setLight(&light);
	scene.addScript(&lightScript);

	scene.addObject(&island1);

	camera.setPosition(glm::vec3(2, 50, 2));
	camera.lookAt(glm::vec3(0, 0, 0));
	camera.setParameters(60, 1, 1000);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);

	glEnable(GL_BLEND); 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glfwSwapInterval(0);

	pndev::Time::init();

	while (!glfwWindowShouldClose(window))
	{
		if (settings.wireframe)
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glDisable(GL_CULL_FACE);
		}
		else
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glEnable(GL_CULL_FACE);
		}

		pndev::Time::calcFrameTime();
		cursorPositionDataUpdate();
		cameraMovement(scene.getActiveCamera(), cameraState.movementState, (float)pndev::Time::getDeltaTime(), cameraState.cameraSpeed);
		cameraRotation(scene.getActiveCamera());

		scene.update();
		scene.render(settings.windowWidth, settings.windowHeight);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
};
#endif